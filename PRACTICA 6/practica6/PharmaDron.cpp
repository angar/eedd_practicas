/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PharmaDron.cpp
 * Author: usuario
 * 
 * Created on 8 de noviembre de 2018, 11:28
 */

#include <set>

#include "PharmaDron.h"

/**
 * @brief Constructor por defecto
 */
PharmaDron::PharmaDron()
    :productos(),
    clientes(),
    posicion(),
    setTerminos(),
    buscaTermino(),
    accesoUTM(),
    drones(){
}
/**
 * COnstructo copia
 * @param orig
 */
PharmaDron::PharmaDron(const PharmaDron& orig)
    :productos(orig.productos),
    clientes(orig.clientes),
    posicion(orig.posicion),
    setTerminos(orig.setTerminos),
    buscaTermino(orig.buscaTermino),
    accesoUTM(orig.accesoUTM),
    drones(orig.drones){
}

PharmaDron& PharmaDron::operator=(const PharmaDron right) {
    if(this !=&right){
        productos=right.productos;
        clientes=right.clientes;
        posicion=right.posicion;
        setTerminos=right.setTerminos;
        buscaTermino=right.buscaTermino;
        accesoUTM = right.accesoUTM;
        drones=right.drones;
    }
    return *this;
}


/**
 * Desctructor
 */
PharmaDron::~PharmaDron() {
}

/**
 * @brief Metodo para cargar los productos en la esctructura de datos de tipo ListaEnalzada 
 * @param fileNameProductos Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::cargaProductos(std::string& fileNameProductos){
    
    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea, le asignamos nuestro PharmaDron
    fe.open(fileNameProductos.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                productos.push_back(lineaProd);
                descomponerStringAniadirSet(lineaProd.getDescripcion());// Añadimos al set, para sabe el tamaño
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
    crearIndice();
}

/**
 * @brief Metodo para cargar los clientes en la esctructura de datos de tipo AVL 
 * le asignamos a cada cliente el PharmaDron (this)
 * @param fileNameClientes Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::crearClientes (std::string &fileNameClientes){
    std::ifstream fe;
    std::string linea;

    Cliente lineaCli(this); //Creamos un cliente para asignar el CSV linea a linea, , le asignamos nuestro PharmaDron
    fe.open(fileNameClientes.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaCli.fromCSV(linea); //Utilizamos el metodo CSV de clientes para rellenar sus atributos
                clientes.insert(std::pair<std::string,Cliente>(lineaCli.GetDni(),lineaCli) );                
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }  
}


/**
 * @brief Metodo para Cargar los clientes y segun sus coordenadas establecer
 *        el tamaño de la malla, añadir productos a clientes y insertar clientes en la malla
 * @param fileNameClientes
 */
void PharmaDron::crearClientesCalculaCoordenadasUtm(std::string& fileNameClientes) {

    
//    if (productos.empty())//Si esta vacio los pructos
//        throw std::length_error("[PharmaDron::crearClientesCalculaCoordenadasUtm]: hay que cargar los productos primero, usar cargaProductos(file)");
//    
    std::ifstream fe;
    std::string linea;
    
    
    float xmin = FLT_MAX, ymin = FLT_MAX, xmax = FLT_MIN, ymax = FLT_MIN;

    Cliente lineaCli(this); //Creamos un cliente para asignar el CSV linea a linea, , le asignamos nuestro PharmaDron
    fe.open(fileNameClientes.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaCli.fromCSV(linea); //Utilizamos el metodo CSV de clientes para rellenar sus atributos
                auto ret = clientes.insert(std::pair<std::string, Cliente>(lineaCli.GetDni(), lineaCli));
                //std::pair<std::map<std::string, Cliente>::iterator,bool> ret
                //sacamos xmin, ymin xmax ymax de los clientes
                if (xmin > ret.first->second.getX())
                    xmin = ret.first->second.getX();
                if (ymin > ret.first->second.getY())
                    ymin = ret.first->second.getY();

                if (xmax < ret.first->second.getX())
                    xmax = ret.first->second.getX();
                if (ymax < ret.first->second.getY())
                    ymax = ret.first->second.getY();
                
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
    
    accesoUTM.ajustarMalla(xmin-AUMENTOVENTANAMALLA, ymin-AUMENTOVENTANAMALLA, 
                           xmax+AUMENTOVENTANAMALLA, ymax+AUMENTOVENTANAMALLA,
                           TAMDIVINICIALX,TAMDIVINICIALY);
    
    //la x-ymin se resta 1 y las x-ymax se suma 1 para que al introducir las coo
    //coordenadas no no s salgamos de la matriz 2d, porque todo lo que se introduzca
    //tiene que ser menor, no puede ser igual, porque al calcular la celda el calculo
    //da la adyacente superior que no existe 
    
    PharmaDron::asignarProductosaClientes();
    PharmaDron::indexarClientesMalla();
}

/**
 * @brief Pense asignar a los clientes los productos justo cuando los metia en el map,
 *        pero de esa manera no se los los clientes que hay para saber el 30% que deben tener productos
 */
void PharmaDron::asignarProductosaClientes() {

    std::srand(std::time(nullptr));

    int sizeClientes = clientes.size();
    int clientconProd = 0;
    //int clienteslleago=0;
    std::list<Producto>::iterator iteProd = productos.begin();



    for (auto i = clientes.begin(); i != clientes.end() && ((float) clientconProd / sizeClientes) < CLIENTESCONPRODU; i++) {
        //clienteslleago++;
        if (std::rand() % NUMRESTO == 0) {//CON EL NUMERO 2 SOLO RECORRO LA MITAD DEL MAPA, CON 3 CASI LLEGA AL FINAL
            i->second.addProductoPedido(&(*iteProd));
            iteProd++;
            clientconProd++;
            if (iteProd == productos.end())
                iteProd = productos.begin();

        }

    }
    std::stringstream cad;
    cad << CLIENTESCONPRODU;
    if (((float) clientconProd / sizeClientes) < CLIENTESCONPRODU)
        throw std::length_error("[PharmaDron::asignarProductosaClientes()]: no se cumple el porcentaje " + cad.str());
    //std::cout<<"clie llegado"<<clienteslleago<<std::endl;

}


/**
 * @brief MEtodo privado para insertar los clientes en la malla
 *        Depues de asignar aletoreamente los productos
 */
void PharmaDron::indexarClientesMalla() {

    for (auto &elem : clientes) {
        accesoUTM.insertar(elem.second.getX(), elem.second.getY(), &elem.second);
    }



}






/**
 * @brief Metodo para añadir un nuevo prodcuto a la lista Enlazada de productos
 * Tambien se añade a la THash
 * Lo pasamos por referencia porque el producto no esta en el sistema
 * Comprueba que el producto no esta repetido (la eficiencia es O(n))
 * @param p
 */
void PharmaDron::aniadeProducto(Producto& p) {
    std::list<Producto>::iterator iteList=productos.begin();
    
    bool esta=false;
    
    while ((iteList != productos.end()) && !esta){
        if(iteList->getCodigo() == p.getCodigo()){
            esta=true;
        }
        iteList++;
    }
    
    if (esta){
        return;
    }else{
          productos.push_back(p);
          descomponerStringAniadirTablaHash(productos.back().getDescripcion(), &productos.back());
          
    }
          
}


/**
 * @brief Metodo para añadir un nuevo cliente al AVL de clientes
 * Inserto en la malla tambien
 * No tiene que ser un puntero porque añado el cliente al sistema
 * @param c
 * @return bool Si se ha llevado con exito o no la insercion
 * @pre Comprobar que el cliente esta en el sistema.
 */
bool PharmaDron::nuevoCliente(Cliente& c) {
    Cliente cli=c;
    cli.SetPharma(this);

    
    std::pair<std::map<std::string,Cliente>::iterator,bool> pairReturn;
    pairReturn=clientes.insert(std::pair<std::string,Cliente>(cli.GetDni(),cli));
    if (pairReturn.second==false)
        return false;
    else{
        accesoUTM.insertar(pairReturn.first->second.getX(),pairReturn.first->second.getY(),&(pairReturn.first->second));
        return true;
    }
}

/**
 * @brief Metodo que busca un Producto en el PharmaDron si coincide con la subcadena
 * @param subcadena
 * @return ListaEnlazada<Producto*> necesito que sean punteros para tenerlos enganchados del sistema
 * @post Si subcadena es la cadena vacia, devuelve una listaEnlazada vacia
 */
std::list<Producto*> PharmaDron::buscaProducto(std::string& subcadena) {

    std::list<Producto*> listaProduc;
    
    if (subcadena != "") {
        std::list<Producto>::iterator iteLisProd = productos.begin();
        while (iteLisProd != productos.end()) {
            if (iteLisProd->getDescripcion().find(subcadena) != std::string::npos)
                listaProduc.push_back(&(*iteLisProd));
            iteLisProd++;
        }
    }
    return listaProduc;    
    
}

/**
 * @brief Metodo para Ingresar un cliente, dado su dni y su pass
 * Si no se da el pass solo comprueba el dni
 * @param dni
 * @param pass
 * @return CLiente* puntero al cliente del sistema buscado 
 *         Sino esta devuelve nullptr 
 */
Cliente* PharmaDron::ingresaCliente(std::string dni, std::string pass) {
    //return &(clientes.at(dni)); //Podriamos usar este metodo pero si no encuentra la clave lanza una excepcion

    Cliente buscado(dni,pass);
    std::map<std::string,Cliente>::iterator iteCliFind;
    if (pass == "") {//Si no se no se nos da pass
        iteCliFind=clientes.find(buscado.GetDni());
        if (iteCliFind != clientes.end())
            return &(iteCliFind->second);
    } else {//Si se da el pass
        iteCliFind=clientes.find(buscado.GetDni());
        if (iteCliFind != clientes.end())
            if(iteCliFind->second.GetPass()==buscado.GetPass())
                return &(iteCliFind->second);
    }
    return nullptr;
}

/**
 * @brief Metodo dado un cliente nos da su pedido si el cliente esta en el sistema
 * @param c
 * @return 
 * @throws std::invalid_argument si no encuentra el cliente
 */
Pedido& PharmaDron::verPedido(Cliente& c) {
    
    Cliente *busc=this->ingresaCliente(c.GetDni(),c.GetPass());
    if (busc!=0)
        return busc->getPedidos();
    else
        throw std::out_of_range("[PharmaDron::verPedido]: el cliente no exite, no se puede ver sus pedidos");
}

/**
 * @brief Metodo para saber el numero de cliente en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numClientes() {
    
    return clientes.size();
}


/**
 * @brief Metodo para visualizar los clientes en la estructura AVL, recorrido inorden.
 */
void PharmaDron::visualizarClientes() {
    std::map<std::string,Cliente>::iterator iteMapCli= clientes.begin();
    int i=1;
    while(iteMapCli !=clientes.end()){
        std::cout<<i++<<": "<<iteMapCli->second<<std::endl;
        iteMapCli++;
    }
    
}

/**
 * @brief Metodo para saber el numero de productos en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numProductos() {
    //return productos.tam();
    return productos.size();
}

/**
 * @brief Metodo para borrar un cliente del sistema dado su dni
 * @param dni
 * @return 
 */
bool PharmaDron::borrarCliente(std::string dni) {

    auto bus = clientes.find(dni);
    if (bus != clientes.end()) {
        accesoUTM.borrar(bus->second.getX(), bus->second.getY(), &bus->second);
        clientes.erase(bus);
        return true;
    }else
        return false;
    //    unsigned long int borrado;
    //    borrado=clientes.erase(dni);//le pasamos la clave, y devuelve un 0 sino borra y un 1 si borra
    //    if (borrado == 0)
    //        return false;
    //    else //borrado 1
    //        return true;
}

///**
// * Necesito la lista de producto para insertarsela en el main para probar THASH
// * @return 
// */
//std::list<Producto>* PharmaDron::dameListaProd() {
//    return &productos;
//}

/**
 * @brief Metodo privado para descomponer la descripcion del producto 
 *        para introducirlo en el std::set
 * @param frase Descripcion del producto
 */
void PharmaDron::descomponerStringAniadirSet(const std::string& frase) {
    std::string pal;

    std::locale loc;
    if (frase != "") {
        std::stringstream aux(frase);
        while (aux >> pal) {
            if (pal.size() >= TAMMAXPALABRA) {
                //                    for(std::string::size_type i =0; i< pal.length(); i++) //std::transform(aux.begin(), aux.end(),aux.begin(), ::tolower)
                //                        pal[i]=std::tolower(pal[i],loc);
                std::transform(pal.begin(), pal.end(), pal.begin(), ::tolower);
                setTerminos.insert(pal);
            }
        }
    }  
}

/**
 * @brief Metodo para calcular el tamaño a partir del std::set
 *       se puede modificar cambiando el FACTORCARGA, 
 *      se ha creado un vector con 90 enteros pero solo funcionan para un rango
 */
void PharmaDron::crearIndice() {
    if (setTerminos.empty())
        throw std::length_error ("[PharmaDron::crearIndice]: set vacio");
    int tamPrimos=90;
    int primos[tamPrimos] = {5387,5393,5399,5407,5413,5417,5419,5431,5437,5441,5443,5449,5471,5477,5479,
                             5483,5501,5503,5507,5519,5521,5527,5531,5557,5563,5569,5573,5581,5591,5623,
                             5639,5641,5647,5651,5653,5657,5659,5669,5683,5689,5693,5701,5711,5717,5737,
                             5741,5743,5749,5779,5783,5791,5801,5807,5813,5821,5827,5839,5843,5849,5851,
                             5857,5861,5867,5869,5879,5881,5897,5903,5923,5927,5939,5953,5981,5987,6007,
                             6011,6029,6037,6043,6047,6053,6067,6073,6079,6089,6091,6101,6113,6121,6131};
    int numTerminos= this->tamsetTerminos();
    int tamTablaHash = std::ceil(numTerminos / (FACTORCARGA)); //TAMAÑO DE LA TABLA SEGUN EL FACTOR QUE INTERESE, ESTE NUMERO NO ES PRIMO
    int i=0;
    for (i=0; i<tamPrimos && tamTablaHash > primos[i] ;i++){   
    }
    if (i == tamPrimos)
        throw std::length_error("[PharmaDron::crearIndice()]: vector de primos pequeños, introducir primos mas grandes ");
    tamTablaHash= primos[i];
    buscaTermino.setTamFisico(tamTablaHash);
    indexaProductos();
}

/**
 * @brief Metodo para indexar Productos a la tabla Hash
 *  Recorremos la lista de productos
 */
void PharmaDron::indexaProductos() {

    std::list<Producto>::iterator iteLisProd = productos.begin();
    while (iteLisProd != productos.end()) {
        this->descomponerStringAniadirTablaHash(iteLisProd->getDescripcion(), &(*iteLisProd));
        iteLisProd++;
    }
    
}

/**
 * @brief Metodo privado para descomponer la descripcion del producto 
 *        para introducirlo en la tablaHash
 * @param frase Descripcion del producto
 * @param p Puntero a producto para añadirlo en el vector de cada termino
 */
void PharmaDron::descomponerStringAniadirTablaHash(const std::string& frase, Producto *p) {
    std::string pal;
    std::locale loc;

    if (frase != "") {
        std::stringstream aux(frase);
        while (aux >> pal) {//!aux.eof() da problemas porque si hay un espacio al final se repite dos veces el ultimo termino
            if (pal.size() >= TAMMAXPALABRA) {
                std::transform(pal.begin(), pal.end(), pal.begin(), ::tolower);
                buscaTermino.insertar(djb2(pal.c_str()), pal, p);
            }
        }
    } 
}

/**
 * @brief Metodo para buscar los productos que en la descripcion se encuentre el termino
 *        La busqueda usando este metodo es muy eficiente.
 * @param termino
 * @return 
 */
std::vector<Producto*> PharmaDron::buscaProductoThas(std::string& termino) {
    if (termino == "")
        throw std::invalid_argument("[PharmaDron::buscaProductoThas]: termino vacio");
    std::transform(termino.begin(), termino.end(), termino.begin(), ::tolower);
    return buscaTermino.buscar(djb2(termino.c_str()), termino);
}

/**
 * @brief Metodo auxiliar para saber el tamaño del set
 * @return tamaño 
 */
int PharmaDron::tamsetTerminos() {
    return setTerminos.size();
}

/**
 * @brief Metodo auxiliar para ver el contenido del set
 */
void PharmaDron::imprimirSEt() {
    std::set<std::string>::iterator it = setTerminos.begin();
    //auto it = setTerminos.begin();
    while (it != setTerminos.end()) {
        std::cout << *it << std::endl;
        it++;
    }
}

/**
 * @brief Metodo para saber el estado de la tablaHash
 */
void PharmaDron::estadoTablaHash() {
    std::cout << "Tam fisico           :" << buscaTermino.tamaTabla() << std::endl;
    std::cout << "Num Terminos(logico) :" << buscaTermino.numTerminos() << std::endl;
    std::cout << "Num Productos        :" << buscaTermino.numProductoss() << std::endl;
    std::cout << "Max colisiones       :" << buscaTermino.maxColisioness() << std::endl;
    std::cout << "Promedio colisiones  :" << buscaTermino.promedioColisiones() << std::endl;
    std::cout << "Factor carga         :" << buscaTermino.factorCarga() << std::endl;
}

/**
 * @brief Funcion para obtener la clave de una palabra
 * @param str puntero al primer caracter de la cadena
 * @return la clave
 */
unsigned int djb2(const char *str) {
    long int hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;
    return hash;
}

///**
// * @brief Metodo auxliar para darme el mapa con los clientes
// * @return 
// */
//std::map<std::string, Cliente>* PharmaDron::dameMapa() {
//    return &clientes;
//
//}

/**
 * @brief Metodo para saber el estado de la malla regular
 */
void PharmaDron::estadoMallaRegular() {
    std::cout << "Max elem celda    :" << accesoUTM.maxElementosPorCelda() << std::endl;
    std::cout << "Max elem fila     :" << accesoUTM.maxElementosPorFila() << std::endl;
    std::cout << "Max elem col      :" << accesoUTM.maxElementosPorColumna() << std::endl;
    std::cout << "Promedio ele celda:" << accesoUTM.promedioElementosPorCelda() << std::endl;
}

/**
 * @brief Metodo privado para obtener los clientes en dicha area
 *        tomando una posicion y un tamaño del recuadro en grados
 * @param pos
 * @param tama
 * @return 
 */
std::vector<Cliente*> PharmaDron::buscaClienteCuadr(UTM &pos, float tama) {
    if (accesoUTM.empty())
        throw std::invalid_argument("[PharmaDron::buscaClienteCuadr]: no se puede hacer busqueda, malla no inicializada");
    float xmin = pos.GetLongitud() - tama;
    float ymin = pos.GetLatitud() - tama;
    float xmax = pos.GetLongitud() + tama;
    float ymax = pos.GetLatitud() + tama;
    return accesoUTM.buscarRangoEficiente(xmin, ymin, xmax, ymax);
}

/**
 * @brief Metodo para reajustar la malla regular, dandole el numero de divisiones
 * @param fil
 * @param col
 */
void PharmaDron::definirNumeroZonas(int fil, int col) {
    if (accesoUTM.empty())
        throw std::invalid_argument("[PharmaDron::buscaClienteCuadr]: no se puede hacer busqueda, malla no inicializada");
    accesoUTM.reajustar(col, fil);
}

/**
 * @brief Metodo para llevar los pedidos en una zona determinada
 * @param d
 * @param pos
 * @param tama
 * @return numero de clientes a lo que se les ha repartido 
 */
int PharmaDron::llevarPedidosZona(Dron* d, UTM& pos, float tama) {

    if (d == nullptr)
        throw std::invalid_argument("[PharmaDron::llevarPedidosZona]: dron no valido");
    std::vector<Cliente*> vectorZona = PharmaDron::buscaClienteCuadr(pos, tama);
    std::cout<<"Id dron " <<d->getId()<<std::endl;
    d->setEstado(Dron::ACTIVO);
    int contClientDron = 0;
    for (int i = 0; i < (int) vectorZona.size(); i++) {
        if (vectorZona.at(i)->getPedidos().numProductos() != 0) {
            d->aniadeCliente(vectorZona.at(i));
            vectorZona.at(i)->getPedidos().setEstado(Pedido::ENTREGADO); //vectorZona.at(i)->getPedidos().entregado();
            contClientDron++;
        }
    }
    return contClientDron;
}

/**
 * @brief Nos da el primer dron disponible
 * @return 
 */
Dron* PharmaDron::getDronDisponible() {
    if (drones.empty())
        throw std::length_error("[PharmaDron::getDronDisponible]: No hay drones, usar metodo generar drones");

    std::list<Dron>::iterator itedron = drones.begin();

    for (; itedron != drones.end(); itedron++) {
        if (itedron->getEstado() == Dron::DISPONIBLE)
            return &(*itedron);
    }
    return nullptr;
}

/**
 * @brief Generamos drones aleaotiros
 */
void PharmaDron::genererarDrones() {
    std::srand(std::time(nullptr));
    /**
     * -10.274, 34.8669, 4.28104, 44.2726
     * latitud min 34.8669
     * longitud min -10.274
     * latitud max 44.2726
     * longitud max 4.28104
     * 
     */

    Dron::ESTADO est;
    for (int i = 0; i < NUMDRONES; i++) {
        float lat = (std::rand() % 35) - 10;
        float longit = (std::rand() % 44) + 5;
        int estado = std::rand() % 2;

        if (estado)//estado es 1
            est = Dron::ACTIVO;
        else
            est = Dron::DISPONIBLE;
        Dron d(i, lat, longit, est);
        drones.push_back(d);
    }
    //std::cout<<drones.size()<<std::endl;

}
