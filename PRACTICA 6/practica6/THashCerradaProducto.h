/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   THashCerradaProducto.h
 * Author: usuario
 *
 * Created on 16 de noviembre de 2018, 9:36
 */

#ifndef THASHCERRADAPRODUCTO_H
#define THASHCERRADAPRODUCTO_H

#include <string> //std::string
#include <vector> //std::vector
#include <cmath> //std::pow, std::ceil
#include <iostream>

#include "Producto.h"

#define TAMMAXCOLISIONES 15
#define PRIMOMENOR 5573//5591 para tam 5737
#define PRIMOANTECESOR 7121//7853


//#define FDISPCUADRATICA //fDispCuadratica
#define FDISPDOBLEXCLASE //fDispDoblexClase
    
//#define FDISPDOBLEXINTERNET //fDispDoblexInternet
//#define FDISPDOBLEXINTERNET1 //fDispDoblexInternet1
//#define FDISPDOBLEXDIVISION //fDispDoblexDivision


/**El tamaño de la tabla debe ser primo PARA QUE FUNCIONE MEJOR
 * 
 */
class THashCerradaProducto {
    //friend class PharmaDron;
private:
    enum Estado {LIBRE, OCUPADO};///< Diferentes Estados que puede tomar la Entrada

private:
    /**Clase entrada */
    class Entrada {
    public:
        long int clave;
        std::string cadena;
        Estado marca;
        std::vector<Producto *> datos;

    public:
        Entrada(long int cla = 0, std::string cad = "", Estado mar = LIBRE);
        Entrada(const Entrada &orig);

        Entrada& operator=(const Entrada &right);
        virtual ~Entrada();
    };

public:
    THashCerradaProducto(long int tamTabla=5);
    THashCerradaProducto(const THashCerradaProducto& orig);
    THashCerradaProducto& operator=(const THashCerradaProducto &right);
    virtual ~THashCerradaProducto();

public:    
    bool insertar (const long int clave, const std::string &termino, Producto *dato);
    std::vector<Producto *> buscar(const long int clave, const std::string &termino);
    void setTamFisico(long int tamfisico);   
    
private:    
    long int fDispCuadratica(const long int clave, long int &intentos); 
    long int fDispDoblexClase(const long int clave, long int &intentos); 
    
    long int fDispDoblexInternet(const long int clave, long int &intentos); 
    long int fDispDoblexInternet1(const long int clave, long int& intentos);
    long int fDispDoblexDivision(const long int clave, long int &intentos);
    
    long int fDis(const long int clave, long int &intentos);

public:    
    long int tamaTabla();
    long int numProductoss();
    long int numTerminos();
    long int maxColisioness();
    long int promedioColisiones();
    float factorCarga();
    
    long int terminosNoInsertados();
    
    
private:
    long int tamFisico;
    long int tamLogico;//==numTerminos
    long int numProductos;
    long int maxColisiones;
    long int totalColisiones;
    long int noInsertados; ///< terminos no insertados
    long int numInserciones;
    std::vector<Entrada> buscaTermino;
};

#endif /* THASHCERRADAPRODUCTO_H */

