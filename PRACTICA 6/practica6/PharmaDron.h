/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PharmaDron.h
 * Author: usuario
 *
 * Created on 8 de noviembre de 2018, 11:28
 */

#ifndef PHARMADRON_H
#define PHARMADRON_H



#include <iostream> //std::cout, std::endl
#include <string>
#include <fstream> //std::ifstream (lectura fichero)
#include <stdexcept> //std::invalid_argument, std::out_of_range
#include <map> //std::map
#include <list> //std::list
#include <set> //std::set
#include <locale> //std::tolower, std::locale
#include <algorithm> //std::transform
#include <cmath> //std::ceil
#include <cfloat> //FLT_MIN, FLT_MAX
#include <cstdlib> //std::srand std::rand()
#include <ctime> //std::time


#include "Cliente.h"
#include "THashCerradaProducto.h"
#include "MallaRegular.h"
#include "Dron.h"

#define TAMMAXPALABRA 4
#define FACTORCARGA 0.63
//CAMBIAR POR 0.61 (5827), 0.62 (5737) Y 0.63 (5639)

#define AUMENTOVENTANAMALLA 1 //VALOR PARA AUMENTAR LA ESQUINA SUPERIOR Y DISMINUIR LA ESQUINA INFERIOR 
#define TAMDIVINICIALX 20 //(columnas)
#define TAMDIVINICIALY 10 //( filas)
#define CLIENTESCONPRODU 0.31
#define NUMRESTO 3
#define NUMDRONES 20


class Cliente;
class Dron;

class PharmaDron {
public:
    PharmaDron();
    PharmaDron(const PharmaDron& orig);
    virtual ~PharmaDron();
    PharmaDron& operator =(const PharmaDron right);
    
    void cargaProductos(std::string &fileNameProductos);
    void crearClientes (std::string &fileNameClientes);
    void crearClientesCalculaCoordenadasUtm(std::string &fileNameClientes);
    void aniadeProducto(Producto &p);//puede ser por referencia o copia porque añadimos al sistema
    bool nuevoCliente (Cliente &c);//puede ser por referencia o copia porque añadimos al sistema
    std::list<Producto *> buscaProducto(std::string &subcadena);
    Cliente* ingresaCliente(std::string dni, std::string pass="");
    Pedido& verPedido(Cliente &c) ; 
    bool borrarCliente(std::string dni);
    
    std::vector<Producto *> buscaProductoThas(std::string &termino);
    
    int numClientes();
    void visualizarClientes();
    
    int numProductos();
    
public://TODO BORRAR METODO
    //std::map<std::string, Cliente>* dameMapa();

private:
    void descomponerStringAniadirSet(const std::string &frase);
    void crearIndice();
    void indexaProductos();
    void descomponerStringAniadirTablaHash(const std::string& frase, Producto *p);
public:    
    int tamsetTerminos();
    void imprimirSEt();
    //std::list<Producto>* dameListaProd();
    void estadoTablaHash();
    
    void estadoMallaRegular();
    void definirNumeroZonas(int fil, int col);
    int llevarPedidosZona(Dron *d, UTM &pos, float tama);
    Dron * getDronDisponible();
    void genererarDrones();
    
    
private:
    void indexarClientesMalla();
    void asignarProductosaClientes();
    std::vector<Cliente*> buscaClienteCuadr(UTM &pos, float tama);
    
private:
    std::list<Producto> productos;///<Composicion de productos EEDD std::list
    std::map<std::string, Cliente> clientes;///<Composicion de clientes std::map
    UTM posicion;///< Posicion del PharmaDron
    std::set<std::string> setTerminos; ///< Estructura set para saber los terminos diferentes para la tHash
    THashCerradaProducto buscaTermino;///<Composicion con producto para la tablaHash
                                       ///< Funciona como un indice para busquedas eficientes 
    MallaRegular<Cliente *> accesoUTM;///< Malla regular de punteros a clientes, es una asociacion
    std::list<Dron> drones;
    
};


unsigned int djb2(const char *str);


#endif /* PHARMADRON_H */

