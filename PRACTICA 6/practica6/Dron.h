/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Dron.h
 * Author: usuario
 *
 * Created on 14 de diciembre de 2018, 1:02
 */

#ifndef DRON_H
#define DRON_H

#include <list>

#include "UTM.h"
#include "Cliente.h"

class Cliente;
class Dron {
public:
    enum ESTADO{ACTIVO, DISPONIBLE};
    
public:   
    Dron(int id_=0, float latitud=0, float longitud=0, ESTADO es= DISPONIBLE);
    Dron& operator=(const Dron &right);
    Dron(const Dron& orig);
    virtual ~Dron();

    void aniadeCliente(Cliente *c);
    void setEstado(ESTADO estado);
    ESTADO getEstado() const;
    int getId() const;
    
    
private:
    int id; ///< IDENTIFICADOR DEL DRON
    UTM pos;///< POSICION UTM DEL DRON
    ESTADO estado;///< ESTADO DEL DRON
    std::list <Cliente *> llevaA;///< ASOCIACION DE CLIENTES A UN DRON
    

};

#endif /* DRON_H */

