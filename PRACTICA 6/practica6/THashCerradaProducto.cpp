/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   THashCerradaProducto.cpp
 * Author: usuario
 * 
 * Created on 16 de noviembre de 2018, 9:36
 */

#include "THashCerradaProducto.h"
/**************************************************************************/
/***                    THashCerradaProducto                            ***/
/**************************************************************************/

/**
 * @brief COnstructor por defecto parametrizado
 * @param tamTabla
 */
THashCerradaProducto::THashCerradaProducto(long int tamTabla) 
    : tamFisico(tamTabla),
    tamLogico(0),
    numProductos(0),
    maxColisiones(0),
    totalColisiones(0),
    noInsertados(0),
    numInserciones(0),    
    buscaTermino(tamTabla){
    
}

/**
 * @brief Constructor copia
 * @param orig
 */
THashCerradaProducto::THashCerradaProducto(const THashCerradaProducto& orig)
    :tamFisico(orig.tamFisico),
    tamLogico(orig.tamLogico),
    maxColisiones(orig.maxColisiones),
    totalColisiones(orig.totalColisiones),
    noInsertados(orig.noInsertados),
    numInserciones(orig.numInserciones),    
    buscaTermino(orig.buscaTermino){
}

/**
 * @brief OPerador asignacion
 * @param right
 * @return 
 */
THashCerradaProducto& THashCerradaProducto::operator=(const THashCerradaProducto& right) {
    if (this != &right){
        tamFisico=right.tamFisico;
        tamLogico=right.tamLogico;
        maxColisiones=right.maxColisiones;
        totalColisiones=right.totalColisiones;
        noInsertados=right.noInsertados;
        numInserciones=right.numInserciones;
        buscaTermino=right.buscaTermino;
    }
    return *this;
}



/**
 * @brief Destructor
 */
THashCerradaProducto::~THashCerradaProducto() {   
}



/**
 * @brief Metodo para buscar en la tablaHash
 *          La buscque termina cuando se cumplen  limite de intentos TAMMAXCOLISIONES
 *          o se encuentra una posicion vacia
 * @param clave
 * @param termino
 * @return vector de productos
 */
std::vector<Producto*> THashCerradaProducto::buscar(const long int clave, const std::string &termino) {
    
    std::vector<Producto *> vacio;

    long int i = 0;
    long int hx = fDispDoblexClase(clave, i);

    if (tamLogico == 0)
        return std::vector<Producto *>();//vacio;
    
    while (buscaTermino[hx].marca == OCUPADO && i < TAMMAXCOLISIONES) {// != LIBRE //&& colisionesInsercion < NUMMAXCOLISIONES
        if (buscaTermino[hx].cadena == termino)//Se comprueba el termino no la clave, el djb2 puede darse casos que para terminos != tengan == clave
            return buscaTermino[hx].datos;
        i++;
        hx = fDispDoblexClase(clave, i);
    }

    return vacio;
}

/**
 * @brief Metodo para insertar en la tablaHash, no es necesario comprobar si esta llena la tabla
 * porque no tiene sentido que este llena, queremos que haya poca colisiones para acercarnos a O(1)
 * utilizo la funcion de dispersion doble
 * La manera de gestionar los terminos no intruducidos es creando un atributo noInsertados, porque
 * queria ver cuantas no se insertaban en vez de lanzar excepcion
 * 
 * Voy a dejar la excepcion comentada por si en un futuro se quiere utilizar este codigo 
 * para saber directamente si el tamaño es correcto o no, entonces aumentar el tamaño de la tabla
 * @param clave
 * @param termino
 * @param dato
 */
bool THashCerradaProducto::insertar(const long int clave, const std::string &termino, Producto* dato) {

    numInserciones++;
    long int colisionesInsercion = 0; //SERIA IGUAL QUE i
    long int hx = 0;

#ifdef FDISPDOBLEXCLASE
    hx = fDispDoblexClase(clave, colisionesInsercion);
#endif

#ifdef FDISPCUADRATICA 
    hx = fDispCuadratica(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXINTERNET
    hx = fDispDoblexInternet(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXINTERNET1
    hx = fDispDoblexInternet1(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXDIVISION
    hx = fDispDoblexDivision(clave, colisionesInsercion);
#endif


    while (buscaTermino[hx].marca != LIBRE && colisionesInsercion < TAMMAXCOLISIONES) {
        //El termino ya existe, lo unico insertamos el producto en el vector
        if (buscaTermino[hx].cadena == termino) {

            buscaTermino[hx].datos.push_back(dato);
            numProductos++;
            //tamLogico++;       no sumamos porque ya exite, solo insertamos en el vector         
            //buscaTermino[hx].marca = OCUPADO; no hace falta poner ocupado porque ya estaba
            //buscaTermino[hx].clave = clave; no hace falta ya estaba

            if (colisionesInsercion > maxColisiones)
                maxColisiones = colisionesInsercion;

            return true;
        }

        totalColisiones++;
        colisionesInsercion++;

#ifdef FDISPDOBLEXCLASE
        hx = fDispDoblexClase(clave, colisionesInsercion);
#endif

#ifdef FDISPCUADRATICA 
        hx = fDispCuadratica(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXINTERNET
        hx = fDispDoblexInternet(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXINTERNET1
        hx = fDispDoblexInternet1(clave, colisionesInsercion);
#endif
#ifdef FDISPDOBLEXDIVISION
        hx = fDispDoblexDivision(clave, colisionesInsercion);
#endif


    }
    //Hemos salido del bucle porque hemos alcanzado el maximo de colisiones o hemos encontrado una libre
    if (colisionesInsercion == TAMMAXCOLISIONES) {
        if (colisionesInsercion > maxColisiones)
            maxColisiones = colisionesInsercion;
        noInsertados++;
        //throw std::length_error("[THashCerradaProducto::insertar]: no insetada")
        return false;
        
    } else {
        tamLogico++;
        buscaTermino[hx].marca = OCUPADO;
        buscaTermino[hx].clave = clave;
        buscaTermino[hx].cadena = termino;
        buscaTermino[hx].datos.push_back(dato);
        ++numProductos;

        if (colisionesInsercion > maxColisiones)
            maxColisiones = colisionesInsercion;
        return true;
    }

}

/**
 * @brief Metodo para cambiar el tamaño de la tabla Hash
 * Se utilza porque en el pharma necesitaba saber antes el tamaño del set
 * Si el usuario, creara un tabla hash y decidiera cambiar el tamaño lanzaria excepcion
 * @pre El metodo resize solo aumenta el tamaño pero cada posicion lo deja con el contenido anterior
 * @param tamfisico
 */
void THashCerradaProducto::setTamFisico(long int tamfisico) {
    if (tamLogico != 0 || tamfisico < 0)
        throw std::invalid_argument("[THashCerradaProducto::setTamFisico]: no se puede cambiar el tamaño");
    
    //DEjamos el vector con Entradas a cero
    for (long unsigned int i=0; i< buscaTermino.size(); i++)
        buscaTermino[i]= Entrada ();
    
    this->tamFisico = tamfisico;
    buscaTermino.resize(tamFisico);
}

/**
 * @brief Funcion de dispersion cuadratica
 * @param clave
 * @param intentos
 * @return 
 */
long int THashCerradaProducto::fDispCuadratica(const long int clave, long int &intentos) {
    //long int i=0;
    return (clave + (long int)std::pow(intentos,2)) % tamFisico;
}

/**
 * @brief Dispersion doble
 * @param clave
 * @param intentos
 * @return 
 */
long int THashCerradaProducto::fDispDoblexClase(const long int clave, long int& intentos) {
    long int h1=clave % PRIMOMENOR;//h1=clave % PRIMOMENOR //h1=(PRIMOMENOR + clave) % PRIMOMENOR
    long int h2= 1 + (clave % PRIMOMENOR);//h2= 1 + (clave % PRIMOMENOR);   //h2= PRIMOMENOR - (clave % PRIMOMENOR);          //h2= PRIMOMENOR + (clave % PRIMOMENOR);
    long int h= (h1 + intentos * h2) % tamFisico;
    return h;
    
    
}
///**
// * @brief Dispersion doble
// * @param clave
// * @param intentos
// * @return 
// */
//long int THashCerradaProducto::fDispDoblexInternet(const long int clave, long int& intentos) {
//    
//    return (clave + intentos * (PRIMOANTECESOR - clave % PRIMOANTECESOR)) %tamFisico;
//}
//
//
///**
// * @brief Funcion dispersion doble
// * @param clave
// * @param intentos
// * @return 
// */
//long int THashCerradaProducto::fDispDoblexInternet1(const long int clave, long int& intentos) {
//    long long int h1= std::floor ((23 * std::pow(clave,2) / PRIMOMENOR));
//    long long int h2= clave * PRIMOMENOR;
//    long long int h= (h1 + intentos * h2) % tamFisico;
//    return h;
//}
//
///**
// * @brief Funcion disersion doble
// * @param clave
// * @param intentos
// * @return 
// */
//long int THashCerradaProducto::fDispDoblexDivision(const long int clave, long int& intentos) {
//    long int h1=(PRIMOMENOR + clave) % PRIMOMENOR;//h1=clave % PRIMOMENOR
//    long int h2= PRIMOMENOR - (clave % PRIMOMENOR);//h2= PRIMOMENOR + (clave % PRIMOMENOR);
//    long int h= (h1 + intentos * h2) % tamFisico;
//    return h;
//}
//
///**
// * @brief Funcion dispersion simple, muy mala, usada para probar la tabla hash
// * @param clave
// * @param intentos
// * @return 
// */
//long int THashCerradaProducto::fDis(const long int clave, long int& intentos) {
//    return (clave* intentos) % tamFisico;
//}



/**
 * @brief Metodo para saber el tamaño Fisico de la tabla
 * @return 
 */
long int THashCerradaProducto::tamaTabla() {
    return tamFisico;
}

/**
 * @Metodo para saber el numero de productos totales
 * @return 
 */
long int THashCerradaProducto::numProductoss() {
    return numProductos;
//    //Se podria hacer un bucle por toda la tabla
//    unsigned long int i=0;
//    for (i=0; i < tamFisico; i++){
//        if (buscaTermino[i].marca == OCUPADO)
//            i += buscaTermino[i].datos.size(); 
//    }
//    return i;
}

/**
 * @brief Metodo para saber el tamaño logico (terminos)
 * @return 
 */
long int THashCerradaProducto::numTerminos() {
    return tamLogico;

}

/**
 * @brief Metodo para saber el maximo de colisiones
 * @return 
 */
long int THashCerradaProducto::maxColisioness() {
    return maxColisiones;
}


/**
 * @brief Metodo para promedio colisiones
 * @return 
 */
long int THashCerradaProducto::promedioColisiones() {
    
    if (tamLogico != 0)
        return std::ceil( totalColisiones / (float) numInserciones);
    return 0;
    
}

/**
 * @brief Metodo para saber el factor de carga entre 0 y 1, mas proximo a 1 mas llena la tabla
 * @return 
 */
float THashCerradaProducto::factorCarga() {
    float fCarga= tamLogico / (float) tamFisico;
    return fCarga;

}

/**
 * @brief Metodo para saber los terminos no insertados
 * @return 
 */
long int THashCerradaProducto::terminosNoInsertados() {
    return noInsertados;
}


/**************************************************************************/
/***                    Entrada                                         ***/
/**************************************************************************/

/**
 * @brief Constructor por defecto parametrizado de Entrada
 * @param cla
 * @param cad
 * @param mar
 */
THashCerradaProducto::Entrada::Entrada(long int cla, std::string cad, Estado mar)
    :clave(cla),
    cadena(cad),
    marca(mar),
    datos(){
}


/**
 * @brief Constructor copia de Entrada
 * @param orig
 */
THashCerradaProducto::Entrada::Entrada(const Entrada& orig)
    :clave(orig.clave),
    cadena(orig.cadena),
    marca(orig.marca),
    datos(orig.datos) {
}


/**
 * @brief Destructor de Entrada
 */
THashCerradaProducto::Entrada::~Entrada() {
}


/**
 * @brief Sobrecarga del operador = de Entrada
 * @param right Objeto del cual copiaremos los atributos
 * @return 
 */
typename THashCerradaProducto::Entrada& THashCerradaProducto::Entrada::operator=(const THashCerradaProducto::Entrada& right) {
    if (this != &right) {
        clave=right.clave;
        cadena=right.cadena;
        marca=right.marca;
        datos=right.datos;
    }
    return *this;
}


