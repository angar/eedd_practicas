/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MallaRegular.h
 * Author: usuario
 *
 * Created on 10 de diciembre de 2018, 19:08
 */

#ifndef MALLAREGULAR_H
#define MALLAREGULAR_H

#include <list> //std::list
#include <vector> //std::vector
#include <cstddef> //nullptr
#include <stdexcept> //std::invalid_argument
#include <iostream> //std::cin std::cout
#include <algorithm> //std::swap<T>()
#include <utility> //std::swap<T>()

template <typename T>
class MallaRegular {
private:

    class Casilla {
        friend class MallaRegular; //Se puede hacer asi para que la malla pueda acceder al atributo o poner puntos public porque desde fuera no se puede acceder ya que casilla es privada
    private:
        std::list<T> puntos;
    public:
        Casilla();
        Casilla(const Casilla &orig);
        //Casilla& operator=(const Casilla &right);
        virtual ~Casilla();
        void insertar(const T &dato);
        T* buscar(const T &dato);
        bool borrar(const T &dato);
    };


public: //Constructores, operador asignacion y destructor
    
    MallaRegular();
    
    MallaRegular(float aXMin, float aYMin, float aXMax,
            float aYMax, int nDivX, int nDivY);
    MallaRegular(const MallaRegular& orig);
    MallaRegular& operator=(const MallaRegular &right);
    virtual ~MallaRegular();

public://Metodos publicos generales
    void insertar(float x, float y, const T &dato);
    T* buscar(float x, float y, const T &dato);
    bool borrar(float x, float y, const T &dato);

public: //Metodos especificos para la practica
    void reajustar (int nDivX, int nDivY);
    void ajustarMalla(float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY);
    std::vector<T> buscarRango(float rXMin, float rYMin, float rXMax, float rYMax);
    std::vector<T> buscarRangoEficiente(float rXMin, float rYMin, float rXMax, float rYMax);
    unsigned int maxElementosPorCelda();
    unsigned int maxElementosPorFila();
    unsigned int maxElementosPorColumna();
    float promedioElementosPorCelda();
    bool empty();
    
private://Metodo para verificar el reajustar       
    int totalElementosMalla();
    int numCasillas();
    int numCasillasVacias();
    
private: //Metodo privado necesario para metodos publicos
    Casilla* obtenerCasilla(float x, float y);

    public: 
        void dibujarMalla();
    
    
private:

    float xMin; ///< Coordenada minima eje X
    float yMin; ///< Coordenada minima eje Y
    float xMax; ///< Coordenada maxima eje X
    float yMax; ///< Coordenada maxima eje Y

    float tamaCasillaX; ///< Tamaño horizontal de la Casilla (eje X)
    float tamaCasillaY; ///< Tamaño vertical de la Casilla (eje Y)

    std::vector< std::vector<Casilla> > mr; ///< Matriz de Casillas
};



/**************************************************************************/
/***                       CASILLA                                      ***/
/**************************************************************************/

/**
 * @brief Constructor por defecto de casilla
 */
template <typename T>
MallaRegular<T>::Casilla::Casilla()
    :puntos(){
}

/**
 * @brief Constructor copia de Casilla
 * @param orig
 */
template <typename T>
MallaRegular<T>::Casilla::Casilla(const Casilla &orig)
    :puntos(orig.puntos){
    
}

/**
 * @brief Operador asginacion de Casilla
 * @param right
 * @return 
 */
//template <typename T>
//typename MallaRegular<T>::Casilla& MallaRegular<T>::Casilla::operator =(const Casilla &right){
//    if (this != &right){
//        puntos=right.puntos;
//    }
//}

/**
 * @brief Destructor de Casilla
 */
template <typename T>
MallaRegular<T>::Casilla::~Casilla(){
}

/**
 * @brief Metodo para insertar el dato al final de la lista enlazada
 * @param dato 
 */
template <typename T>
void MallaRegular<T>::Casilla::insertar(const T& dato){
    puntos.push_back(dato);
}


/**
 * @brief Metodo para buscar el dato dentro de la lista enlazada
 * @param dato
 * @return 
 * @pre Necesario el operador == del tipo T 
 */
template <typename T>
T* MallaRegular<T>::Casilla::buscar(const T& dato){
    typename std::list<T>::iterator ite=puntos.begin();
    while (ite != puntos.end()){
        if (*ite == dato)
            return &(*ite);
        ite++;
    }
    return nullptr;
}


/**
 * @brief Metodo para buscar el dato dentro de la lista enlazada
 * @param dato
 * @return true o false dependiendo si se borra el dato o no
 * @pre Necesario el operador == del tipo T 
 */
template <typename T>
bool MallaRegular<T>::Casilla::borrar(const T& dato){
    typename std::list<T>::iterator ite=puntos.begin();
    for( ; ite != puntos.end(); ite++){
        if (*ite == dato){
            puntos.erase(ite);
            return true;
        }    
    }
    return false;
}






/**************************************************************************/
/***                       MALLA REGULAR                                ***/
/**************************************************************************/

template <typename T>
MallaRegular<T>::MallaRegular()
    :xMin(0),
    yMin(0),
    xMax(0),
    yMax(0),
    tamaCasillaX(0),
    tamaCasillaY(0),
    mr(){   
}

/**
 * @brief Constructor parametrizado de malla regular
 * @param aXMin
 * @param aYMin
 * @param aXMax
 * @param aYMax
 * @param nDivX
 * @param nDivY
 */
template <typename T>
MallaRegular<T>::MallaRegular(float aXMin, float aYMin, float aXMax, 
                 float aYMax, int nDivX, int nDivY)
    :xMin(aXMin),
    yMin(aYMin),
    xMax(aXMax),
    yMax(aYMax){
    tamaCasillaX=(xMax - xMin) / nDivX;
    tamaCasillaY=(yMax - yMin) / nDivY;
    mr.insert(mr.begin(), nDivY, std::vector<MallaRegular<T>::Casilla>(nDivX)); // Desde el primer elemento, meter nDivY(numero de filas) veces un vector de tamaño nDivX (columnas)
}

/**
 * @brief Constructor copia de malla regular
 * @param orig
 */
template <typename T>
MallaRegular<T>::MallaRegular(const MallaRegular& orig)
    :xMin(orig.xMin),
    yMin(orig.yMin),
    xMax(orig.xMax),
    yMax(orig.yMax),
    tamaCasillaX(orig.tamaCasillaX),
    tamaCasillaY(orig.tamaCasillaY),
    mr(orig.mr){
}
/**
 * @brief Operador = (asignacion) de malla regular
 * @param right
 * @return 
 */
template <typename T>
MallaRegular<T>& MallaRegular<T>::operator=(const MallaRegular &right) {
    if (this != &right) {
        xMin = right.xMin;
        yMin = right.yMin;
        xMax = right.xMax;
        yMax = right.yMax;
        tamaCasillaX = right.tamaCasillaX;
        tamaCasillaY = right.tamaCasillaY;
        mr = right.mr;
    }
    return *this;
}

/**
 * @brief Destructor de malla regular
 */
template <typename T>
MallaRegular<T>::~MallaRegular() {
}

/**
 * @brief Metodo para insertar un dato en la malla
 * @param x
 * @param y
 * @param dato
 * @return 
 * @throw std::length_error, std::invalid_argument, std::out_of_range 
 *        Lanza excepcion tanto si la posicion no es correcta de la matriz y si los parametros
 *        fuera del rango, y si se intenta buscar en la malla vacia
 */
template <typename T>
void MallaRegular<T>::insertar(float x, float y, const T& dato) {
    if (mr.empty())
        throw std::length_error("[MallaRegular<T>::insertar]: malla vacia, sin inicializar ");


    Casilla *casillaBus = obtenerCasilla(x, y);
    if (casillaBus != nullptr) { //No seria necesario porque obtener casilla lanza excepcion 
        casillaBus->insertar(dato);
    }

}

/**
 * @brief Metodo para buscar el dato en la malla regular
 * @param x
 * @param y
 * @param dato
 * @return Devuelve un puntero al tipo de dato, si no lo encuentra nullptr
 * @pre Necesario el operador == del tipo T 
 * @throw std::length_error, std::invalid_argument, std::out_of_range 
 *        Lanza excepcion tanto si la posicion no es correcta de la matriz y si los parametros
 *        fuera del rango, y si se intenta buscar en la malla vacia
 */
template <typename T>
T* MallaRegular<T>::buscar(float x, float y, const T& dato) {
    if (mr.empty())
        throw std::length_error("[MallaRegular<T>::buscar]: malla vacia, sin inicializar ");

    Casilla *casillaBus = obtenerCasilla(x, y);
    return casillaBus->buscar(dato);
}

/**
 * @brief Borrar un dato de la malla regular, borra el dato de la casilla
 * @param x
 * @param y
 * @param dato
 * @return true si el dato es borrado, false si el dato no es borrado, porque
 * no se encuentre
 * @throw std::length_error, std::invalid_argument, std::out_of_range 
 *        Lanza excepcion tanto si la posicion no es correcta de la matriz y si los parametros
 *        fuera del rango, y si se intenta buscar en la malla vacia
 */
template <typename T>
bool MallaRegular<T>::borrar(float x, float y, const T& dato) {
    if (mr.empty())
        throw std::length_error("[MallaRegular<T>::borrar]: malla vacia, sin inicializar ");

    Casilla *casillaBus = obtenerCasilla(x, y);
    return casillaBus->borrar(dato);
}

/**
 * @brief Obtener una Casilla dadas sus coordenadas, como obtener casilla se usa en todos los metodos
 *        lanzamos la excepcion aqui en vez en el metodo de buscar, borrar e insertar
 * @param x
 * @param y
 * @return Casilla segun las coordenadas
 * @throw Lanza excepcion tanto si la posicion no es correcta de la matriz y si los parametros
 *        fuera del rango
 */
template <typename T>
typename MallaRegular<T>::Casilla* MallaRegular<T>::obtenerCasilla(float x, float y) {
    //No puede ser igual porque al hacer  if ((x >= xMin && x <= xMax) && (y >= yMin && y <= yMax)) porque 
    //al hacer el calculo si fuera justo el borde exterior, al hacer el calculo intenta acceder una posicion mas y el vector no tiene
    if ((x > xMin && x < xMax) && (y > yMin && y < yMax)) {
        int j = (x - xMin) / tamaCasillaX; //Obtenemos el numero de columna
        int i = (y - yMin) / tamaCasillaY; //Obtenemso el numero de fila()
        return &(mr.at(i).at(j));
    } else
        throw std::invalid_argument("[MallaRegular<T>::obtenerCasilla]: rango x,y invalido");

}

/**
 * @brief
 * @param nDivX
 * @param nDivY
 * @pre El metodo funciona solo para objetos de tipo puntero, ademas el objeto debe
 * tener implementado el metodo getX(), getY()
 * @throw std::length_error Si se intenta reajustar una malla instanciada con el constructor por defecto
 */
template <typename T>
void MallaRegular<T>::reajustar(int nDivX, int nDivY) {
    if (mr.empty()) {
        throw std::length_error("[MallaRegular<T>::reajustar]: no se puede usar Metodo reajustar porque esta vacia, usar ajustarMalla ");
    }

    MallaRegular<T> newMalla(this->xMin, this->yMin, this->xMax, this->yMax, nDivX, nDivY);
    
    for (int i = 0; i < (int) mr.size(); i++) {
        for (int j = 0; j < (int) mr.at(i).size(); j++) {
            if (!mr.at(i).at(j).puntos.empty()) {//Si no es vacia recorro la lista enlazada
                typename std::list<T>::iterator iteLisPuntos = mr.at(i).at(j).puntos.begin();
                for (; iteLisPuntos != mr.at(i).at(j).puntos.end(); iteLisPuntos++) {
                    newMalla.insertar((*iteLisPuntos)->getX(), (*iteLisPuntos)->getY(), *iteLisPuntos);
                }
            }
        }
    }

    

    std::swap(newMalla,*this);//Esto no funciona
    //Los otros 4 atributos no es necesario porque no se modifican
//    std::swap(newMalla.tamaCasillaX, this->tamaCasillaX);
//    std::swap(newMalla.tamaCasillaY, this->tamaCasillaY);
//    std::swap(newMalla.mr, this->mr);

}

/**
 * @brief Metodo para ajustar la malla, usarlo despues si se utiliza el constructor por defecto
 *        si la malla estuviera llena ajusta los elementos a las nuevas dimensiones
 * @param aXMin
 * @param aYMin
 * @param aXMax
 * @param aYMax
 * @param nDivX
 * @param nDivY
 * @pre El metodo funciona solo para objetos de tipo puntero, ademas el objeto debe
 * tener implementado el metodo getX(), getY()
 * @post Si la malla es inicilizada con el constructor por defecto, al ajustar la malla
 * el contenido de la malla sera vacio, no tiene ningun elemento
 * 
 */
template <typename T>
void MallaRegular<T>::ajustarMalla(float aXMin, float aYMin, float aXMax, float aYMax, int nDivX, int nDivY) {
    MallaRegular<T> newMalla(aXMin, aYMin, aXMax, aYMax, nDivX, nDivY);


    if (!mr.empty()) {
        for (int i = 0; i < (int) mr.size(); i++) {
            for (int j = 0; j < (int) mr.at(i).size(); j++) {
                if (!mr.at(i).at(j).puntos.empty()) {//Si no es vacia recorro la lista enlazada
                    typename std::list<T>::iterator iteLisPuntos = mr.at(i).at(j).puntos.begin();
                    for (; iteLisPuntos != mr.at(i).at(j).puntos.end(); iteLisPuntos++) {
                        newMalla.insertar((*iteLisPuntos)->getX(), (*iteLisPuntos)->getY(), *iteLisPuntos);
                    }
                }
            }
        }
    }
    
    std::swap(newMalla,*this);
    
//    std::swap(newMalla.xMin, this->xMin);
//    std::swap(newMalla.yMin, this->yMin);
//    std::swap(newMalla.xMax, this->xMax);
//    std::swap(newMalla.yMax, this->yMax);
//    std::swap(newMalla.tamaCasillaX, this->tamaCasillaX);
//    std::swap(newMalla.tamaCasillaY, this->tamaCasillaY);
//    std::swap(newMalla.mr, this->mr);

}

/**
 * @brief Busqueda de rango, no es del todo eficiente, recorre todo el vector 2D
 * @param rXMin
 * @param rYMin
 * @param rXMax
 * @param rYMax
 * @return 
 * @pre El metodo funciona solo para objetos de tipo puntero, ademas el objeto debe
 * tener implementado el metodo getX(), getY()
 * @throw std::invalid_argument Si estan fuera de rango la zona
 * @post si la malla no esta inicilizada devuelve un vector vacio
 */
template <typename T>
std::vector<T> MallaRegular<T>::buscarRango(float rXMin, float rYMin, float rXMax, float rYMax) {
    std::vector<T> vec;


    if (mr.size() == 0)
        return std::vector<T>();

    if (rXMin <= xMin || rYMin <= yMin || rXMax >= xMax || rYMax >= yMax)
        throw std::invalid_argument("[MallaRegular<T>::buscarRango]: fuera de rango");

    for (int i = 0; i < (int) mr.size(); i++) {
        for (int j = 0; j < (int) mr[i].size(); j++) {
            if (!mr.at(i).at(j).puntos.empty()) {//Si no es vacia recorro la lista enlazada, esta comprobacion no seria necesaria porque el iterador si esta vacia es igual el principio que el final
                typename std::list<T>::iterator iteLisPuntos = mr.at(i).at(j).puntos.begin();
                for (; iteLisPuntos != mr.at(i).at(j).puntos.end(); iteLisPuntos++) {
                    if ((*iteLisPuntos)->getX() >= rXMin && (*iteLisPuntos)->getX() <= rXMax &&
                            (*iteLisPuntos)->getY() >= rYMin && (*iteLisPuntos)->getY() <= rYMax)
                        vec.push_back(*iteLisPuntos);
                }
            }
        }

    }
    return vec;

}

/**
 * @brief Busqueda de rango eficiente, solo recorre las casillas que corresponden
 *        con el tamaño de de la zona
 * @param rXMin
 * @param rYMin
 * @param rXMax
 * @param rYMax
 * @return vector<T> 
 * @pre El metodo funciona solo para objetos de tipo puntero, ademas el objeto debe
 *      tener implementado el metodo getX(), getY()
 * @throw std::invalid_argument Si estan fuera de rango la zona
 * @post si la malla no esta inicilizada devuelve un vector vacio
 */
template <typename T>
std::vector<T> MallaRegular<T>::buscarRangoEficiente(float rXMin, float rYMin, float rXMax, float rYMax) {
    std::vector<T> vec;


    if (mr.size() == 0)
        return std::vector<T>();

    if (rXMin <= xMin || rYMin <= yMin || rXMax >= xMax || rYMax >= yMax)//No puede ser igual porque se saldra del vector 2D
        throw std::invalid_argument("[MallaRegular<T>::buscarRango]: fuera de rango");


    //Calcular celda inferior (de rXMin, rYMIn)
    int jXMin = (rXMin - xMin) / tamaCasillaX; //Obtenemos el numero de columna
    int iYMin = (rYMin - yMin) / tamaCasillaY; //Obtenemso el numero de fila()
    //std::cout<<"ij min"<<iYMin<<','<<jXMin<<std::endl;

    //Calcular celda superior (de rXMin, rYMIn)
    int jXMax = (rXMax - xMin) / tamaCasillaX; //Obtenemos el numero de columna
    int iYMax = (rYMax - yMin) / tamaCasillaY; //Obtenemso el numero de fila()
    //std::cout<<"ij Max"<<iYMax<<','<<jXMax<<std::endl;

    //Se pone <= porque son posiciones de la matriz de 0 a tam-1
    for (int i = iYMin; i <= iYMax; i++) {
        for (int j = jXMin; j <= jXMax; j++) {
            typename std::list<T>::iterator iteLisPuntos = mr.at(i).at(j).puntos.begin();
            for (; iteLisPuntos != mr.at(i).at(j).puntos.end(); iteLisPuntos++) {
                if ((*iteLisPuntos)->getX() >= rXMin && (*iteLisPuntos)->getX() <= rXMax && //aqui si puede ser = porque estamos dentro de todo el vector 2D
                        (*iteLisPuntos)->getY() >= rYMin && (*iteLisPuntos)->getY() <= rYMax)
                    vec.push_back(*iteLisPuntos);
            }

        }

    }
    return vec;

}




/**
 * @NOTA : PARA LOS METODOS DE RECORRER LA MATRIZ SE HA UTILIZADO CORCHETES
 *          PORQUE SE HA DEPURADO QUE FUNCIONARA CORRECTAMENTE, PERO LA PRIMERA 
 *          VEZ QUE SE IMPLEMENTARO SE USO .at(i).at(j), para evitar errores o corregir
 *          por posibles fallos de incrementar fila o columna erroneamente
 */

/**
 * @brief Obener el maximo de elementos en una celda
 * @return numero de elementos
 * @pre no es necesario usar at. porque no se pasan parametros, ni hay que comprobar
 */
template <typename T>
unsigned int MallaRegular<T>::maxElementosPorCelda() {
    if (mr.size() == 0)
        return 0;

    unsigned int max = 0;
    //std::cout<< "tamaño fila"<<mr.size()<<std::endl;
    //std::cout<< "tamaño col"<<mr[0].size()<<std::endl;

    for (int i = 0; i < (int) mr.size(); i++)
        for (int j = 0; j < (int) mr[i].size(); j++) //mr.at(i).size();
            if (mr[i][j].puntos.size() > max)//if ( mr.at(i).at(j).puntos.size() > max ) //mr.at(i).at(j) == mr[i][j] mejor usar at si se pasan parametros porque lanza excepcion
                max = mr[i][j].puntos.size();

    return max;
}

/**
 * @brief Metodo para saber el maximo de elementos de una fila de la malla regular
 * @return numero maximo de elementos en una fila
 */
template <typename T>
unsigned int MallaRegular<T>::maxElementosPorFila() {
    if (mr.size() == 0)
        return 0;


    unsigned int maxEleFila = 0;
    unsigned int contadorFila = 0;
    for (int i = 0; i < (int) mr.size(); i++) {
        contadorFila = 0;
        for (int j = 0; j < (int) mr[i].size(); j++)
            contadorFila += mr[i][j].puntos.size();

        if (contadorFila > maxEleFila)
            maxEleFila = contadorFila;
    }
    //std::cout<<"maxElefila " <<maxEleFila<<std::endl; 
    return maxEleFila;
}

/**
 * @brief Metodo para saber el maximo de elementos de una columna de la malla regular
 * @return numero maximo de elementos en una columna
 */
template <typename T>
unsigned int MallaRegular<T>::maxElementosPorColumna() {
    if (mr.size() == 0)
        return 0;

    unsigned int maxEleCol = 0;
    unsigned int contadorCol = 0;
    //Dejamos fija la j y movemos la i
    for (int j = 0; j < (int) mr[j].size(); j++) {//se desborda el unsgined long int al llegar a 100
        contadorCol = 0;
        for (int i = 0; i < (int) mr.size(); i++)
            contadorCol += mr[i][j].puntos.size(); //mr[i][j].puntos.size() //mr.at(i).at(j).puntos.size();

        //std::cout<<"j "<<j <<"mrsize"<<mr[j].size() <<"contadorCol " <<contadorCol<<std::endl;
        if (contadorCol > maxEleCol)
            maxEleCol = contadorCol;
    }
    return maxEleCol;
}

/**
 * @brief Metodo para saber el promedio de elementos por celda
 * @return promedio 
 */
template <typename T>
float MallaRegular<T>::promedioElementosPorCelda() {

    if (mr.size() == 0)
        return 0;

    unsigned int contEleCeldas = 0;

    for (int i = 0; i < (int) mr.size(); i++)
        for (int j = 0; j < (int) mr[i].size(); j++)
            contEleCeldas += mr[i][j].puntos.size();

    unsigned int celdas = mr.size() * mr[0].size();

    return (contEleCeldas / (float) celdas);
}

/**
 * @brief Metodo para saber el total de elementos en la malla
 *        Seria mejor tener un contador que incrementemos o decrementemos dentro de la malla
 *        al insertar en vez de recorrecer el vector 2D, ya que esto tiene eficiencia O(n²)
 * @return Elementos totales de la celda 
 */
template <typename T>
int MallaRegular<T>::totalElementosMalla() {

    if (mr.size() == 0)
        return 0;

    unsigned int contEleCeldas = 0;

    for (int i = 0; i < (int) mr.size(); i++)
        for (int j = 0; j < (int) mr[i].size(); j++)
            contEleCeldas += mr[i][j].puntos.size();


    return contEleCeldas;
}

/**
 * @brief Metodo para saber el total de casillas de la malla
 * @return Elementos totales de la celda 
 */
template <typename T>
int MallaRegular<T>::numCasillas() {

    if (mr.size() == 0)
        return 0;
    //Tamaño de la columna * por el tamaño de la primera fila
    return mr.size() * mr.at(0).size();
}

/**
 * @brief Metodo para saber el numero de casillas vacias
 * @return NUmero entero de casillas sin elementos
 */
template <typename T>
int MallaRegular<T>::numCasillasVacias() {

    if (mr.size() == 0)
        return 0;

    int contCasVacia = 0;
    for (int i = 0; i < (int) mr.size(); i++)
        for (int j = 0; j < (int) mr[i].size(); j++)
            if (mr[i][j].puntos.empty())
                contCasVacia++;
    return contCasVacia;

}
/**
 * @brief Metodo para mostrar las casillas de la malla si estan ocupadas o vacias
 *        - casilla vacia
 *        * casilla ocupada
 * Se visualiza bien si las divisiones son menores de 20
 * @return NUmero entero de casillas sin elementos
 */
template <typename T>
void MallaRegular<T>::dibujarMalla() {

    if (mr.size() == 0)
        return ;

    
    for (int i = 0; i < (int) mr.size(); i++){
        for (int j = 0; j < (int) mr[i].size(); j++)
            if (mr[i][j].puntos.empty())
                std::cout<<'-'<<'\t';
            else
                std::cout<<'*'<<'\t';
        std::cout<<std::endl;
    }
}

/**
 * @brief Metodo para saber si esta inicializada la malla regular
 * @return true si esta vacia, false si no esta vacia 
 */
template <typename T>
bool MallaRegular<T>::empty(){
    if (mr.empty())
        return true;
    else
        return false;
}

#endif /* MALLAREGULAR_H */

