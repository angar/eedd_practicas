/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Dron.cpp
 * Author: usuario
 * 
 * Created on 14 de diciembre de 2018, 1:02
 */

#include "Dron.h"

/**
 * @brief Constructor por defecto parametrizado de dron
 * @param id_
 * @param latitud
 * @param longitud
 * @param es
 */
Dron::Dron(int id_, float latitud, float longitud, ESTADO es) 
    :id(id_),
    pos(latitud,longitud),
    estado(es),
    llevaA(){
}

/**
 * @brief Constructor copia de dron
 * @param orig
 */
Dron::Dron(const Dron& orig)
    :id(orig.id),
    pos(orig.pos),
    estado(orig.estado),
    llevaA(orig.llevaA){
}

/**
 * @brief Operador asigancion =, dron
 * @param right
 * @return 
 */
Dron& Dron::operator=(const Dron& right) {
    if(this != &right){
        id=right.id;
        pos=right.pos;
        estado=right.estado;
        llevaA=right.llevaA;
    }
    return *this;
}


/**
 * @brief Destructor dron
 */
Dron::~Dron() {
}

/**
 * @brief Añadimos el cliente por el final
 * @param c
 */
void Dron::aniadeCliente(Cliente* c) {
    llevaA.push_back(c);
}

void Dron::setEstado(ESTADO estado) {
    this->estado = estado;
}

Dron::ESTADO Dron::getEstado() const {
    return estado;
}

int Dron::getId() const {
    return id;
}

