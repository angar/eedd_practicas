/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */

//#include <cstdlib>
#include "Producto.h"
#include"Cliente.h"
#include"Pedido.h"
#include "PharmaDron.h"
#include "THashCerradaProducto.h"
#include "MallaRegular.h"

#include <iostream>
//#include <climits>
#include <cfloat> //FLT_MIN, FLT_MAX
#include <fstream> //std::ifstream (lectura fichero)
#include <clocale>//std::setlocale
#include <cstring>
#include <stdexcept>
#include <exception>
#include <cmath>

#define PRECIO 50

using namespace std;


void visualizaPedido(Pedido &l);

int main(int argc, char** argv) {
    try {
        std::setlocale(LC_ALL, "es_ES.UTF8");

        string nomFichProduc = "pharma1.csv";
        string nomFichCLien = "clientes_v2.csv";
        //    /**************************************************************************/
        //    /***                     APARTADO 1º                                    ***/
        //    /**************************************************************************/
        /**Establecer el número inicial de zonas con un número de filas y columnas cualquiera, p.e. 10 filas y 20 columnas.*/
        //En pharmaDron #define TAMDIVINICIALX 20 //(columnas) #define TAMDIVINICIALY 10 //( filas)
        PharmaDron empresaPharma;
        empresaPharma.cargaProductos(nomFichProduc);
        empresaPharma.crearClientesCalculaCoordenadasUtm(nomFichCLien);

        //    /**************************************************************************/
        //    /***                     APARTADO 2º                                    ***/
        //    /**************************************************************************/    
        empresaPharma.genererarDrones();
        Dron *d = empresaPharma.getDronDisponible();
        UTM segovia(40.941992, -4.109535);
        cout << "Repartos Segovia: " << empresaPharma.llevarPedidosZona(d, segovia, 0.25) << endl << endl;

        //    /**************************************************************************/
        //    /***                     APARTADO 3º                                    ***/
        //    /**************************************************************************/        
        empresaPharma.estadoMallaRegular();
        empresaPharma.definirNumeroZonas(22, 80);
        cout << endl;
        empresaPharma.estadoMallaRegular();

        //    /**************************************************************************/
        //    /***                     APARTADO 4º                                    ***/
        //    /**************************************************************************/  
        cout << endl;
        d = empresaPharma.getDronDisponible();
        UTM madrid(40.414826, -3.701364);
        cout << "Repartos Segovia: " << empresaPharma.llevarPedidosZona(d, segovia, 0.35) << endl << endl;

        //    /**************************************************************************/
        //    /***                     APARTADO 5º                                    ***/
        //    /**************************************************************************/  
        empresaPharma.definirNumeroZonas(24, 22);
        cout << endl;
        cout << "conseguimos bajar de 50" << endl;
        empresaPharma.estadoMallaRegular();


        //    /**************************************************************************/
        //    /***                     APARTADO 6º                                    ***/
        //    /**************************************************************************/  
        cout << endl;
        d = empresaPharma.getDronDisponible();
        UTM jaen(37.777909, -3.786051);
        cout << "Repartos Segovia: " << empresaPharma.llevarPedidosZona(d, segovia, 0.3) << endl << endl;

        //    /**************************************************************************/
        //    /***                     APARTADO 6º                                    ***/
        //    /**************************************************************************/     
        //Este apartado voy a ajustar para que cada celda tenga maximo 10 elementos, para que
        //la busqueda sea proxima a O(1)
        empresaPharma.definirNumeroZonas(74 ,72);
        cout << endl;
        cout << "conseguimos bajar de 10" << endl;
        empresaPharma.estadoMallaRegular();
        
        //    //https://www.geogebra.org/m/Wk7Y7N6V
        //
        //    //MallaRegular<Cliente*> malla;
        //    MallaRegular<Cliente*> malla(-10.274, 34.8669, 4.28104, 44.2726, 100, 100);




    } catch (std::bad_alloc &e) {
        std::cerr << "Fallo reserva memoria " << e.what() << endl;
        throw e;

    } catch (std::invalid_argument &e) {
        std::cerr << e.what() << endl;
        throw e;

    } catch (std::exception &e) {
        std::cerr << e.what() << endl;
    }
    return 0;

}

void visualizaPedido(Pedido &l) {
    cout << "/*************************/" << endl;
    cout << "/*   Datos del pedido   **/" << endl;
    cout << "/*************************/" << endl;
    cout << endl;
    //    cout << "/***Detalles de productos *******/" << endl;
    //    for (int i = 0; i < l.numProductos(); i++) {
    //        cout << l.getProducto(i).toCSV() << endl;
    //    }
    cout << "/***Total productos *******/" << endl;
    cout << l.numProductos() << endl;
    cout << endl;
    cout << "/***Precio total compra *******/" << endl;
    cout << l.importe() << " €" << std::endl;
}
