/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PharmaDron.cpp
 * Author: usuario
 * 
 * Created on 8 de noviembre de 2018, 11:28
 */

#include "PharmaDron.h"

/**
 * @brief Constructor por defecto
 */
PharmaDron::PharmaDron()
    :productos(),
    clientes(),
    posicion(){
}
/**
 * COnstructo copia
 * @param orig
 */
PharmaDron::PharmaDron(const PharmaDron& orig)
    :productos(orig.productos),
    clientes(orig.clientes),
    posicion(orig.posicion){
}

PharmaDron& PharmaDron::operator=(const PharmaDron right) {
    if(this !=&right){
        productos=right.productos;
        clientes=right.clientes;
        posicion=right.posicion;
    }
    return *this;
}


/**
 * Desctructor
 */
PharmaDron::~PharmaDron() {
}

/**
 * @brief Metodo para cargar los productos en la esctructura de datos de tipo ListaEnalzada 
 * @param fileNameProductos Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::cargaProductos(std::string& fileNameProductos){
    
    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea, le asignamos nuestro PharmaDron
    fe.open(fileNameProductos.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                productos.push_back(lineaProd);
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}

/**
 * @brief Metodo para cargar los clientes en la esctructura de datos de tipo AVL 
 * le asignamos a cada cliente el PharmaDron (this)
 * @param fileNameClientes Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::crearClientes (std::string &fileNameClientes){
    std::ifstream fe;
    std::string linea;

    Cliente lineaCli(this); //Creamos un cliente para asignar el CSV linea a linea, , le asignamos nuestro PharmaDron
    fe.open(fileNameClientes.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaCli.fromCSV(linea); //Utilizamos el metodo CSV de clientes para rellenar sus atributos
                clientes.insert(std::pair<std::string,Cliente>(lineaCli.GetDni(),lineaCli) );                
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }    
}

/**
 * @brief Metodo para añadir un nuevo prodcuto a la lista Enlazada de productos
 * Lo pasamos por referencia porque el producto no esta en el sistema
 * Comprueba que el producto no esta repetido (la eficiencia es O(n))
 * @param p
 */
void PharmaDron::aniadeProducto(Producto& p) {
    std::list<Producto>::iterator iteList=productos.begin();
    
    bool esta=false;
    
    while ((iteList != productos.end()) && !esta){
        if(iteList->getCodigo() == p.getCodigo()){
            esta=true;
        }
        iteList++;
    }
    
    if (esta){
        return;
    }else
          productos.push_back(p);
}


/**
 * @brief Metodo para añadir un nuevo cliente al AVL de clientes
 * No tiene que ser un puntero porque añado el cliente al sistema
 * @param c
 * @return bool Si se ha llevado con exito o no la insercion
 * @pre Comprobar que el cliente esta en el sistema.
 */
bool PharmaDron::nuevoCliente(Cliente& c) {
    Cliente cli=c;
    cli.SetPharma(this);

    
    std::pair<std::map<std::string,Cliente>::iterator,bool> pairReturn;
    pairReturn=clientes.insert(std::pair<std::string,Cliente>(cli.GetDni(),cli));
    if (pairReturn.second==false)
        return false;
    else
        return true;
}

/**
 * @brief Metodo que busca un Producto en el PharmaDron si coincide con la subcadena
 * @param subcadena
 * @return ListaEnlazada<Producto*> necesito que sean punteros para tenerlos enganchados del sistema
 * @post Si subcadena es la cadena vacia, devuelve una listaEnlazada vacia
 */
std::list<Producto*> PharmaDron::buscaProducto(std::string& subcadena) {

    std::list<Producto*> listaProduc;
    
    if (subcadena != "") {
        std::list<Producto>::iterator iteLisProd = productos.begin();
        while (iteLisProd != productos.end()) {
            if (iteLisProd->getDescripcion().find(subcadena) != std::string::npos)
                listaProduc.push_back(&(*iteLisProd));
            iteLisProd++;
        }
    }
    return listaProduc;    
    
}

/**
 * @brief Metodo para Ingresar un cliente, dado su dni y su pass
 * Si no se da el pass solo comprueba el dni
 * @param dni
 * @param pass
 * @return CLiente* puntero al cliente del sistema buscado 
 *         Sino esta devuelve nullptr 
 */
Cliente* PharmaDron::ingresaCliente(std::string dni, std::string pass) {
    //return &(clientes.at(dni)); //Podriamos usar este metodo pero si no encuentra la clave lanza una excepcion

    Cliente buscado(dni,pass);
    std::map<std::string,Cliente>::iterator iteCliFind;
    if (pass == "") {//Si no se no se nos da pass
        iteCliFind=clientes.find(buscado.GetDni());
        if (iteCliFind != clientes.end())
            return &(iteCliFind->second);
    } else {//Si se da el pass
        iteCliFind=clientes.find(buscado.GetDni());
        if (iteCliFind != clientes.end())
            if(iteCliFind->second.GetPass()==buscado.GetPass())
                return &(iteCliFind->second);
    }
    return nullptr;
}

/**
 * @brief Metodo dado un cliente nos da su pedido si el cliente esta en el sistema
 * @param c
 * @return 
 * @throws std::invalid_argument si no encuentra el cliente
 */
Pedido& PharmaDron::verPedido(Cliente& c) {
    
    Cliente *busc=this->ingresaCliente(c.GetDni(),c.GetPass());
    if (busc!=0)
        return busc->getPedidos();
    else
        throw std::out_of_range("[PharmaDron::verPedido]: el cliente no exite, no se puede ver sus pedidos");
}

/**
 * @brief Metodo para saber el numero de cliente en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numClientes() {
    
    return clientes.size();
}


/**
 * @brief Metodo para visualizar los clientes en la estructura AVL, recorrido inorden.
 */
void PharmaDron::visualizarClientes() {
    std::map<std::string,Cliente>::iterator iteMapCli= clientes.begin();
    int i=1;
    while(iteMapCli !=clientes.end()){
        std::cout<<i++<<": "<<iteMapCli->second<<std::endl;
        iteMapCli++;
    }
    
}

/**
 * @brief Metodo para saber el numero de productos en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numProductos() {
    //return productos.tam();
    return productos.size();
}

bool PharmaDron::borrarCliente(std::string dni) {
    unsigned long int borrado;
    borrado=clientes.erase(dni);//le pasamos la clave, y devuelve un 0 sino borra y un 1 si borra
    if (borrado == 0)
        return false;
    else //borrado 1
        return true;
}
