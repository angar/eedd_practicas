/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */

//#include <cstdlib>
#include "Producto.h"
#include"Cliente.h"
#include"Pedido.h"
#include "PharmaDron.h"


#include <iostream>
//#include <climits>
#include <fstream> //std::ifstream (lectura fichero)
#include <clocale>//std::setlocale
#include <cstring>
#include <stdexcept>
#include <exception>



using namespace std;

//Practica 1
//void mostrarVDinamico(VDinamico<Producto>& vec) ;

//Practica 2
//void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l);
void visualizaPedido(Pedido &l);


//Practica 3
void pruebaArbol(void);

int main(int argc, char** argv) {


    cout << "/***********************************************/" << endl;
    cout << "/****        PRACTICA 4 MAP                 ****/" << endl;
    cout << "/***********************************************/" << endl << endl;

    std::setlocale(LC_ALL, "es_ES.UTF8");
    try {
        /**************************************************************************/
        /***                     APARTADO 1º                                    ***/
        /**************************************************************************/
        pruebaArbol();


        /**************************************************************************/
        /***                     APARTADO 2º                                    ***/
        /**************************************************************************/
        cout << endl << endl;
        cout << "/****        PROGRAMA PRUEBA PRACTICA 4                ****/" << endl;
        string nomFichProduc = "pharma1.csv";
        string nomFichCLien = "clientes.csv";

        PharmaDron empresaPharma;
        empresaPharma.cargaProductos(nomFichProduc);
        empresaPharma.crearClientes(nomFichCLien);

        cout << "TAMAÑO DE clientes en empresaPharma:" << empresaPharma.numClientes() << endl;
        cout << "TAMAÑO DE productos en empresaPharma:" << empresaPharma.numProductos() << endl << endl;

        Cliente angel("77777777D", "angel garcia garcia");
        if (empresaPharma.nuevoCliente(angel))
            cout << "Cliente insertado" << endl;
        else
            cout << "Cliente insertado" << endl;

        Cliente *clifind = empresaPharma.ingresaCliente(angel.GetDni());

        cout << "TAMAÑO DE clientes en empresaPharma:" << empresaPharma.numClientes() << endl;



        string almax = "ALMAX";
        std::list<Producto *> listPrAlm;
        if (clifind != nullptr)
            listPrAlm = clifind->buscarProducto(almax);

        cout << "\nTAM lista de ALMAX: " << listPrAlm.size() << endl;

        //        std::list<Producto *>::iterator iteListProd=listPrAlm.begin();
        //        while (iteListProd != listPrAlm.end()) {
        //            cout<<(*iteListProd)->toCSV()<<endl;
        //            iteListProd++;
        //        }

        Producto *prodMin;
        if (clifind != nullptr) {
            prodMin = clifind->buscaProductoMinimo(listPrAlm);
            clifind->addProductoPedido(prodMin);
        }
        //        if (prodMin !=nullptr)
        //            cout<<prodMin->toCSV()<<endl;



        string algidol = "ALGIDOL";
        std::list<Producto *> listPrAl;
        if (clifind != nullptr)
            listPrAl = clifind->buscarProducto(algidol);
        cout << "TAM lista de ALGIDOL: " << listPrAl.size() << endl << endl;

        if (clifind != nullptr) {
            prodMin = clifind->buscaProductoMinimo(listPrAl);
            clifind->addProductoPedido(prodMin);
        }

        clifind = empresaPharma.ingresaCliente(angel.GetDni());

        if (clifind != nullptr)
            visualizaPedido(clifind->getPedidos());

        cout << "\nBorramos cliente" << endl;
        if (empresaPharma.borrarCliente(angel.GetDni()))
            cout << "Cliente borrado" << endl;
        else
            cout << "Cliente no borrado" << endl;
        cout << "TAMAÑO DE clientes en empresaPharma:" << empresaPharma.numClientes() << endl;
        clifind = empresaPharma.ingresaCliente(angel.GetDni());

        if (clifind == nullptr)
            cout << "Cliente no encontrado" << endl << endl << endl;



        /**************************************************************************/
        /***                     APARTADO 2º Practica Anterior adaptada al map  ***/
        /**************************************************************************/
        
        cout << "/****        PROGRAMA PRUEBA PRACTICA 3(ADAPTADA A MAP        ****/" << endl;
        PharmaDron gestion = empresaPharma;

        cout << "TAMAÑO DE clientes en PharmaDron:" << gestion.numClientes() << endl;
        cout << "TAMAÑO DE productos en PharmaDron:" << gestion.numProductos() << endl << endl;
        //gestion.visualizarClientes();
        string dni = "43954678C"; //valido posicion en csv 1655 (nombre: Roderic Wickman)con la primera linea de informacion eliminada
        string _100ML = "100ML";
        string crema = "CREMA";
        std::list<Producto *> listaProduct1;
        std::list<Producto *> listaProduct2;
        std::list<Producto *> listaProduct3;
        std::list<Producto *>::iterator iteLisProdPunteros;

        Cliente *buscado = 0;
        buscado = gestion.ingresaCliente(dni);

        //INSERTAMOS LOS PRODUCTOS DE 100 ML AL CLIENTE RODERIC
        //auto i=buscado->buscarProducto(_100ML);
        if (buscado != 0) {
            listaProduct1 = buscado->buscarProducto(_100ML);
            //cout<<"TAM lista de 100 ml: "<<listaProduct.tam()<<endl;

            iteLisProdPunteros = listaProduct1.begin();

            while (iteLisProdPunteros != listaProduct1.end()) {
                buscado->addProductoPedido(*iteLisProdPunteros);
                iteLisProdPunteros++;
            }
            cout << endl << "*****CLIENTE 1******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }



        //INSERTAMOS LOS PRODUCTOS DE CREMA AL CLIENTE 34923452L Abbi Kirvin POSICION CSV 10000
        dni = "34923452L";
        buscado = gestion.ingresaCliente(dni);


        if (buscado != 0) {
            listaProduct2 = buscado->buscarProducto(crema);
            //cout<<"TAM lista de crema: "<<listaProduct.tam()<<endl;

            iteLisProdPunteros = listaProduct2.begin();

            while (iteLisProdPunteros != listaProduct2.end()) {
                buscado->addProductoPedido(*iteLisProdPunteros);
                iteLisProdPunteros++;
            }
            cout << endl << "*****CLIENTE 2******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }



        //INSERTAMOS LOS PRODUCTOS DE CREMA o 100ML AL CLIENTE 10982609X Marlie Klassmann POSICION CSV 1
        dni = "10982609X";
        buscado = gestion.ingresaCliente(dni);


        if (buscado != 0) {
            //listaProduct3 = listaProduct1.concatena(listaProduct2);
            listaProduct3 = listaProduct1;
            listaProduct3.merge(listaProduct2);
            //cout<<"TAM lista de: "<<listaProduct3.size()<<endl;

            iteLisProdPunteros = listaProduct3.begin();

            while (iteLisProdPunteros != listaProduct3.end()) {
                buscado->addProductoPedido(*iteLisProdPunteros);
                iteLisProdPunteros++;
            }
            cout << endl << "*****CLIENTE 3******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }

        Cliente angel1("77777777D", "angel garcia garcia");
        //gestion.verPedido(angel);
        gestion.nuevoCliente(angel1);
        cout << "TAMAÑO DE clientes en PharmaDron:" << gestion.numClientes() << endl;
        //angel.SetDni("00463415C");
        gestion.nuevoCliente(angel1);
        cout << "TAMAÑO DE clientes en PharmaDron:" << gestion.numClientes() << endl;
        gestion.borrarCliente("77777777D");
        cout << "TAMAÑO DE clientes en PharmaDron:" << gestion.numClientes() << endl;



    } catch (std::bad_alloc &e) {
        std::cerr << "Fallo reserva memoria " << e.what() << endl;
        throw e;

    } catch (std::invalid_argument &e) {
        std::cerr << e.what() << endl;
        throw e;

    } catch (std::exception &e) {
        std::cerr << e.what() << endl;
    }
    return 0;
}


//void mostrarVDinamico(VDinamico<Producto>& vec) {
//    for (unsigned int i = 0; i < vec.tam(); i++)
//        cout <<"Producto "<< i << ": " << vec[i].toCSV() << endl;
//}
//
//void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l){
//    ListaEnlazada<Producto>::Iterador iteListProd=l.iterador();
//    int i=1;
//    while (!iteListProd.fin()){
//        cout<<"Producto "<<i++<<": "<<iteListProd.dato().toCSV()<<endl;
//        iteListProd.siguiente();
//    }
//        
//}

void visualizaPedido(Pedido &l) {
    cout << "/*************************/" << endl;
    cout << "/*   Datos del pedido   **/" << endl;
    cout << "/*************************/" << endl;
    cout << endl;
    cout << "/***Detalles de productos *******/" << endl;
    for (int i = 0; i < l.numProductos(); i++) {
        cout << l.getProducto(i).toCSV() << endl;
    }
    cout << endl;
    cout << "/***Precio total compra *******/" << endl;
    cout << l.importe() << " €" << std::endl;
}

void pruebaArbol(void) {

    /*Se declarará un map<int,int> en el que se introducirán un millón de números 
     * enteros consecutivos. Se retornará la altura del árbol, y se harán dos 
     * búsquedas, una con resultado positivo y otra de un número que no esté .
     */

    //    //1 Forma de insertar, no recomendable
    //    //Este encuentra dentro del map la clava y le asigna el dato
    //    //Sino lo encuentra crea un nuevo elemento con la clave y el dato
    //    for(int i=0;i <20;i++){
    //        arbolint[i]=i;  
    //    }
    //     //2 Forma de insertar 
    //    Si al inserta la clave ya se encuentra, no inserta, no modifica el valor
    //    for(int i=0;i <20;i++){
    //        arbolint.insert(make_pair(i,i));  
    //    }        
    cout << "/****        map<int,int>                 ****/" << endl;
    map<int, int> arbolint;

    //Insertamos 
    //Si al inserta la clave ya se encuentra, no inserta no modifica el valor
    for (int i = 0; i < 1000000; i++) {
        arbolint.insert(pair<int, int>(i, i));
    }

    //    //Forma de recorrer el map
    //    map<int,int>::iterator iteArbolin;
    //    iteArbolin=arbolint.begin();
    //    while (iteArbolin != arbolint.end()){
    //        cout<<iteArbolin->second<<endl;
    //        iteArbolin++;
    //    }


    cout << "Elementos map enteros: " << arbolint.size() << endl;

    int claveBuscada = 500;
    map<int, int>::iterator iteMapInt;

    //cout<<"Dato buscado: "<<arbolint.at(claveBuscada);//Sino lo encuentra lanza excepcion
    //cout<<"Dato buscado: "<<arbolint[claveBuscada];//CUIADO el operador corchete sino encuentra, inserta el dato con la clave
    //cout << "Elementos map enteros: " << arbolint.size() << endl; //vemos que aumenta en 1 el tamaño

    iteMapInt = arbolint.find(claveBuscada);
    if (iteMapInt != arbolint.end())
        cout << "Dato encontrado(clave: " << iteMapInt->first << ") : " << iteMapInt->second << endl;
    else
        cout << "Dato no encontrado, no exite clave: " << claveBuscada << endl;

    claveBuscada = 5000000;
    iteMapInt = arbolint.find(claveBuscada);
    if (iteMapInt != arbolint.end())
        cout << "Dato encontrado(clave: " << iteMapInt->first << ") : " << iteMapInt->second << endl;
    else
        cout << "Dato no encontrado, no exite clave: " << claveBuscada << endl;


}