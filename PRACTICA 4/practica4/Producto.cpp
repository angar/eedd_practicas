/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Producto.cpp
 * Author: admin
 * 
 * Created on 21 de septiembre de 2018, 9:44
 */

#include "Producto.h"

/**
 * @brief Constructor por defecto parametrizado
 * Asigna a los atributos,  codigo "" , descripcion "" y pvp 0 
 * ("" quiere decir que sus atributos estan vacios )
 * 
 * @param cod Codigo del producto
 * @param desc Descripcion del producto
 * @param _pvp Precio del producto
 */
Producto::Producto(std::string cod, std::string desc, float _pvp) 
: codigo(cod),
descripcion(desc),
pvp(_pvp) {

    if (_pvp < 0)
        pvp=0;


}

/**
 * @brief Constructor copia 
 * @param orig Obejo de la clase producto del que se copian los atributos
 */
Producto::Producto(const Producto& orig)
: codigo(orig.codigo),
descripcion(orig.descripcion),
pvp(orig.pvp) {

}

/**
 * @brief Operador de asignacion 
 * @param right Objeto del que se copian los atributos
 * @return Una referencia al propio objeto, para garantizar 
 * la asignacion en cascada (a=b=c)
 */
Producto& Producto::operator=(const Producto& right) {
    if (this != &right) {
        codigo = right.codigo;
        descripcion = right.descripcion;
        pvp = right.pvp;
    }
    return *this;
}

/**
 * @brief Destructor
 */
Producto::~Producto() {

}

void Producto::setPvp(float pvp) throw (std::domain_error) {

    if (pvp < 0)
        throw std::domain_error("[Producto::setPvp]: El precio no puede ser negativo");


    this->pvp = pvp;
}

float Producto::getPvp() const {
    return pvp;
}

void Producto::setDescripcion(std::string descripcion) {
    this->descripcion = descripcion;
}

std::string Producto::getDescripcion() const {
    return descripcion;
}

void Producto::setCodigo(std::string codigo) {
    this->codigo = codigo;
}

std::string Producto::getCodigo() const {
    return codigo;
}

/**
 * @brief Asigna informacion a un producto a partir de su representacion CSV
 * @param linea 
 * @post El campo pvp convertimos de cadena de caracteres numericos a tipo flotante
 */
void Producto::fromCSV(std::string& linea) {
    std::stringstream prod(linea); //Objeto de la clase stringstream para descomponer una cadena de caracteres en formato CSV
    std::string campo; //Cada campo que vayamos leyendo lo guardaremos en esta variable

    //Leemos el codigo
    std::getline(prod, codigo, ';'); //El caracter ';' se lee y se elimina de prod


    //Leemos la descripcion
    std::getline(prod, descripcion, ';');

    //Leemos el precio
    //Primero lo leemos como una cadena de caracteres, despues lo convertimos a real
    //si no fuera posible la conversion stof lanza excepcion de tipo std::invalid_argument
    //capturamos la excepcion que lanza  porque no fuera un campo numerico le asignamos el valor 0
    std::getline(prod, campo, ';');
    try {
        pvp = std::stof(campo);
    } catch (std::invalid_argument &e) {
        pvp = 0;
    }

}

/**
 * @brief Representacion CSV de un producto
 * @return 
 */
std::string Producto::toCSV() {
    std::stringstream aux;
    std::string linea;

    aux << codigo << ";"
            << descripcion << ";"
            << pvp << ";";

    return aux.str();
}
