/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UTM.cpp
 * Author: usuario
 * 
 * Created on 7 de noviembre de 2018, 23:35
 */

#include "UTM.h"

/**
 * @brief COntructor por defecto parametrizado
 * @param lat
 * @param longit
 */
UTM::UTM(float lat,float longit)
    :latitud(lat),
     longitud(longit){
}

/**
 * @brief Constructor copia de UTM
 * @param orig
 */
UTM::UTM(const UTM& orig)
    :latitud(orig.latitud),
     longitud(orig.longitud){
}

/**
 * @brief Operador de asignacion de UTM
 * @param right
 * @return 
 */
UTM& UTM::operator =(const UTM& right){
    if(this !=&right){
        latitud=right.latitud;
        longitud=right.longitud;
    }
    return *this;
}

float UTM::GetLongitud() const {
    return longitud;
}

float UTM::GetLatitud() const {
    return latitud;
}

void UTM::SetLongitud(float longitud) {
    this->longitud = longitud;
}

void UTM::SetLatitud(float latitud) {
    this->latitud = latitud;
}


/**
 * @brief Destructor de UTM
 */
UTM::~UTM() {
}

/**
 * @brief Representacion CVS de una coordenada UTM
 * @return std::string 
 */
std::string UTM::toCSV() const{
    std::stringstream aux;

    aux << latitud << ";"
        << longitud;

    return aux.str();
}


/**
 * @brief Asigna informacion a una coordenada UTM a partir de su representacion CSV
 * @param linea 
 * @post Los campos latitud y longitud convertimos de cadena de caracteres numericos a tipo flotante
 */
void UTM::fromCSV(const std::string& linea){
    std::stringstream coord(linea); //Objeto de la clase stringstream para descomponer una cadena de caracteres en formato CSV
    std::string campo; //Cada campo que vayamos leyendo lo guardaremos en esta variable
    
    
    std::getline(coord, campo, ';');
    try {
        latitud=std::stof(campo);
    } catch (std::invalid_argument &e) {
        latitud=0;
    }

    std::getline(coord, campo, ';');
    try {
        longitud=std::stof(campo);
    } catch (std::invalid_argument &e) {
        longitud=0;
    }    
    
    
    
}


