/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UTM.h
 * Author: usuario
 *
 * Created on 7 de noviembre de 2018, 23:35
 */

#ifndef UTM_H
#define UTM_H

#include <string> //std::string, std::stof
#include <sstream> //std::stringstream para el CSV

class UTM {
public:
    UTM(float lat=0,float longit=0);
    UTM(const UTM& orig);
    virtual ~UTM();
    
    UTM& operator=(const UTM &right);
    float GetLongitud() const;
    float GetLatitud() const;
    void SetLongitud(float longitud);
    void SetLatitud(float latitud);
    
    std::string toCSV() const;
    void fromCSV(const std::string &linea) ;
    
    
    
private:
    float latitud;///< latitud de la coordenada UTM
    float longitud;///< longitud de la coordenada UTM

};

#endif /* UTM_H */

