/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pedido.cpp
 * Author: usuario
 * 
 * Created on 22 de octubre de 2018, 22:16
 */

#include "Pedido.h"


/**
 * @brief Constructor por defecto parametrizado
 * @param ide Identificador del pedido
 * @param est Estado del pedido
 */
Pedido::Pedido(std::string ide, ESTADO est)
    :id(ide),
    estado(est),
    cesta()
    {   
}



/**
 * @brief Constructor copia
 * @param orig
 */
Pedido::Pedido(const Pedido& orig)
    :id(orig.id),
    estado(orig.estado),
    cesta(orig.cesta)
    {
}


/**
 * @brief OPerador de asignacion
 * @param right
 * @return 
 */
Pedido& Pedido::operator =(const Pedido& right){
    if (this != &right){
        id=right.id;
        estado=right.estado;
        cesta=right.cesta;
    }
    return *this;
}

/**
 * @brief Destructor de pedido
 */
Pedido::~Pedido() {
}

/**
 * @brief Metodo para obtener el importe total de la cesta(el pedido)
 * @return precio total del pedido
 */
float Pedido::importe() {
    float importe = 0;
    for (int i = 0; i < (int) cesta.size(); i++)
        importe += cesta[i]->getPvp();
    return importe;
}

/**
 * @brief Metodo para introducir un nuevo producto en la cesta
 * @param prod
 * @pre El producto debe ser valido para insertarse (prod != nullptr), sino no se inserta
 */
void Pedido::nuevoProducto(Producto* prod) {
    if (prod != 0) {
        cesta.push_back(prod);
    }
}

/**
 * @brief Metodo que nos dice el numero de producto que hay en el pedido
 * @return el numero de productos
 */
int Pedido::numProductos(){
    return cesta.size();
}


/**
 * @brief Metodo que devuelve el producto dada una posicion
 *        Si la posicion no fuera valida el vector dinamico lanzaria una excepcion
 * @param i Posicion del procuto dentro del vector dinamico
 * @return Una referencia del producto dentro de la cesta 
 * @throw std::out_of_range Si esta fuera de los limites del vector
 */
Producto& Pedido::getProducto(int i){
    return  *(cesta.at(i)); //*(cesta[i]) el operador [] no comprueba si la i esta dentro de los limites 
}


