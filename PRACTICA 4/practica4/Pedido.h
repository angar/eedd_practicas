/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pedido.h
 * Author: usuario
 *
 * Created on 22 de octubre de 2018, 22:16
 */

#ifndef PEDIDO_H
#define PEDIDO_H


#include"Producto.h"

#include<string> //std::stof
#include<stdexcept> //std::invalid_argument
#include <vector> //std::vector

class Pedido {
public:
    enum ESTADO{PAGADO, ENALMACEN, ENTRANSITO, ENTREGADO} ;//Diferentes estados que puede tener un pedido
    
    Pedido(std::string ide = "", ESTADO est = ENALMACEN);
    Pedido(const Pedido& orig);
    virtual ~Pedido();
    Pedido& operator=(const Pedido &right);
    
    void nuevoProducto(Producto *prod);
    float importe();
    int numProductos();
    Producto& getProducto(int i);

private:
    std::string id; ///< Identificador del pedido
    ESTADO estado;///<  Estado del pedido {PAGADO, ENALMACEN, ENTRANSITO, ENTREGADO}
    std::vector<Producto *> cesta;///< Vector de Producto*, establece la asociacion    
};

#endif /* PEDIDO_H */

