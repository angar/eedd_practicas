/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VDinamico.h
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 10:12
 */

#ifndef VDINAMICO_H
#define VDINAMICO_H


#include<climits>//UINT_MAX esta definida en esta biblioteca
#include<cmath> //para el redondeo ceil
#include<stdexcept> // std::domain_error std::invalid_argument std::length_error std::out_of_range

//#include <iostream> //std::cout
//#include "Producto.h"

template<class T>
class VDinamico {
public:

    VDinamico() throw (std::bad_alloc);
    VDinamico(unsigned int tam) throw (std::bad_alloc, std::invalid_argument);
    VDinamico(const VDinamico& orig) throw (std::bad_alloc);
    VDinamico(const VDinamico& orig, unsigned int inicio, unsigned int num) throw (std::bad_alloc, std::invalid_argument); //Constructor copia parcial
    VDinamico& operator=(const VDinamico& orig) throw (std::bad_alloc);
    virtual ~VDinamico();
    T& operator[](unsigned int pos); //Los valores iran desde 0 a tamL-1 para su correcto funcionamiento

    unsigned int tam();

    void insertar(const T& dato, unsigned int pos = UINT_MAX);
    T borrar(unsigned int pos = UINT_MAX); //Se devuelve una copia por lo tanto es necesario tener constructor copia

private: ///Metodos internos de VDinamico no accesibles desde fuera de la clase
    void calculotamF();
    void disminuye();

private:
    unsigned int tamL, tamF; ///> Tamaño logico y tamaño fisico del vector
    T *v; ///> VEctor de objetos de tipo T
};

/**
 * @brief Constructor por defecto inicia tamL 0 y tamF a 1
 */
template<class T>
VDinamico<T>::VDinamico() throw (std::bad_alloc)
: tamL(0),
tamF(1) {
    v = new T[tamF];
}

/**
 * @brief Constructor parametrizado
 * @param tam Tamaño logico inicial del vector
 * @post Calcula el tamaño físico a la potencia de 2 inmediatamente superior a tam (tamaño lógico)
 */
template<class T>
VDinamico<T>::VDinamico(unsigned int tam) throw (std::bad_alloc, std::invalid_argument)
: tamL(tam) {

    if (tam == 0 || tam == UINT_MAX)
        throw std::invalid_argument("[VDinamico<T>::VDinamico(tam)]No se puede inicializar con valores negativos o cero"); //Si a un unsigned int se le pasa un numero negativo toma el valor de UINT_MAX(el mayor numero que puede almacenar ese tipo de dato)

    this->calculotamF();

    v = new T[tamF];
}

/**
 * @brief Constructor copia
 * Copia tamL y tamF y crea un vector identico al de orig
 * 
 * @param orig Objeto de tipo T del que se asignaran los atributos
 * @pre El tipo T tiene que tener sobrecargado el operador de asignacion (operator=)
 */
template<class T>
VDinamico<T>::VDinamico(const VDinamico& orig) throw (std::bad_alloc)
: tamL(orig.tamL),
tamF(orig.tamF) {

    v = new T[tamF];

    for (int i = 0; (unsigned int) i < tamL; i++)
        v[i] = orig.v[i]; // Necesario sobrecargar operador de asignacion en este caso de producto


}

/**
 * @brief Constructor copia parcial
 * 
 *  No he utilizado en la secuencia de incializacion VDinamico(num) porque se pueden dar condiciones
 *  que no son correctas 
 * Inicio tiene que ser desde 0-tamL-1, obligo que se copien los objetos dentro del tamL
 * 
 * @param orig Obejot del cual se quiere copiar
 * @param inicio La posicion inicial del vector de la cual se copiaran los objetos
 * @param num El numero de objetos a copiar dentro del vector
 * @pre El tipo T tiene que tener sobrecargado el operador de asignacion (operator=)
 * 
 */
template<class T>
VDinamico<T>::VDinamico(const VDinamico& orig, unsigned int inicio, unsigned int num) throw (std::bad_alloc, std::invalid_argument) {
    //Aqui no se comprueba que sea != this porque el constructor copia solo se llama al instanciar el objeto

    if ((inicio + num) >= orig.tamL)
        throw std::invalid_argument("[VDinamico<T>::VDinamico(copia parcial)]: nos excedemos de las posiciones a copiar");
    if (inicio >= orig.tamL || inicio == UINT_MAX)
        throw std::invalid_argument("[VDinamico<T>::VDinamico(copia parcial)]: la posicion de inicio esta fuera del rango");
    if (num == 0 || num == UINT_MAX)
        throw std::invalid_argument("[VDinamico<T>::VDinamico(copia parcial)]: el numero a copiar no puede ser cero ni negativo");

    tamL = num;
    this->calculotamF();
    v = new T[tamF];

    for (int i = 0; i < num; i++) {
        v[i] = orig.v[inicio + i]; // Necesario sobrecargar operador de asignacion en este caso de producto

    }

}

/**
 * 
 * @param orig
 * @return 
 */
template<class T>
VDinamico<T>& VDinamico<T>::operator=(const VDinamico& orig) throw (std::bad_alloc) {

    if (this != &orig) { //1 Comprobar que no sea el mismo objeto, 2 liberar memoria de this, 3 reservar la memoria de objeto derecho y asignar cada objeto
        tamL = orig.tamL;
        tamF = orig.tamF;

        delete [] this->v; //Liberamos el vector de this
        v = new T[tamF]; //Reservamos un nuevo vector del tamaño de orig

        for (int i = 0; i < tamL; i++)
            v[i] = orig.v[i]; //Necesario operador asignacion del tipo T   
    }
    return *this;
}

/**
 * @brief Destructor
 * 
 * Libera la memoria dinamica utilizada
 */
template<class T>
VDinamico<T>::~VDinamico() {
    delete [] v;

}

/**
 * @brief Operador corchetes Para acceder al objeto de la posicion pos
 * 
 * Considero que solo se puede devolver hasta el tamaño Logico(0-tamL-1),
 * porque el usuario le interesa los objeto que tiene, no las posiciones vacias
 * 
 * @param pos Posicion del vector
 * @return Una referencia de tipo T para lectura/escritura (El objeto devuelto podra usarse tanto para saber su contenido como para modificarlo)
 * 
 */
template<class T>
T& VDinamico<T>::operator[](unsigned int pos) {
    if (pos >= tamL || pos == UINT_MAX) {
        throw std::invalid_argument("[VDinamico::operator[]]: no se puede pasar valores negativos "); //Si se le pasa un valor negativo pos(por ser unsigned int) toma el valor de UINT_MAX, y pos no puede ser mayor o igual que el tamF por que internamente el vector su rango es [0,tamL-1]
    }
    return v[pos]; //Devuelve una referencia, no se llama al constructor copia. Si devolviera T (sin referencia) seria necesario sobrecargar el constructor copia del tipo T
}

/**
 * @brief Metodo para consultar el tamaño logico
 * @return El tamaño logico del VDinamico
 */
template<class T>
unsigned int VDinamico<T>::tam() {
    return tamL;
}

/**
 * @brief Metodo para insertar un objeto de tipo T en el vector
 * 
 * @param dato Objeto a insertar en el vector
 * @param pos La posicion en la que se quiere insertar
 */
template<class T>
void VDinamico<T>::insertar(const T& dato, unsigned int pos) {
    if (pos > tamL && pos != UINT_MAX) { //Si se pasara un numero negativo pos valdria UINT_MAX, no pongo igual porque se le puede pasar que inserte al final dandole la posicion
        throw std::out_of_range("[VDinamico<T>::insertar]: Posicion no permitida");
    }

    //Primero comprobar que hay capacidad para intruducir el nuevo dato
    if (tamL == tamF) { //Si no hay espacio para otro objeto el tamaño Fisico aumenta el doble
        tamF *= 2;
        T *vAux = new T[tamF]; //vector auxiliar de tipo T con el nuevo tamaño fisico
        for (unsigned int i = 0;  i < tamL; i++)
            vAux[i] = v[i];
        delete [] v; //liberamos memoria de nuestro vector
        v = vAux; //asignamos la direccion de memoria de aux a v
    }


    if (pos == UINT_MAX) { //insercion al final
        v[tamL++] = dato; //primero inserta el dato en la ultima posicion y despues incrementamos tamL
    } else { //Sino insercion en posicion dada
        for (int i = tamL - 1; i > (int)pos; i++) //Bucle para abrir un hueco en esa posicion (desplazar a la derecha a partir de esa posicion)
            v[i + 1] = v[i];
        v[pos] = dato;
        tamL++;
    }


}

/**
 * @brief Metodo para borrar el objeto de una posicion
 * 
 * 1º Borramos despues comprobamos si tenemos que reducir el tamaño FISICO
 * @param pos La posicion que se quiere borrar
 * @return Copia del Objeto de tipo T que se ha borrado
 */
template<class T>
T VDinamico<T>::borrar(unsigned int pos) {
    if (pos >= tamL) //Si se pasara un numero negativo pos valdria UINT_MAX, es necesario poner el igual porque si hay 10 elementos va de 0-9 no se puede borrar el elemento 10
        throw std::out_of_range("[VDinamico<T>::borrar]: Posicion no permitida");

    T aux;
    if (pos == UINT_MAX) { //borrado al final
        return v[--tamL];

    } else { //Sino borrado en posicion dada
        aux = v[pos]; //1 copio valores antes de borrar
        for (unsigned int i = pos; i < tamL; i++)
            v[i] = v[i + 1];
    }
    tamL--;
    this->disminuye();
    return aux;

}

/**
 * @brief Metodo para calcular el tamaño Fisico del vector
 */
template<class T>
void VDinamico<T>::calculotamF() {
    unsigned int exp;
    exp = std::ceil(std::log2(tamL) / std::log2(2)); //Calculamos el exponente redondeando al entero superior ---tamL < 2^x ---
    //log2 (tamL) < x * log2 (2)-> log2(tamL)/log2(2)<x

    tamF = (tamL < std::pow(2, exp)) ? std::pow(2, exp) : std::pow(2, exp + 1); //Utilizamos el operador condicional ternario para comprobar que el tamaño fisico sea superior al logico 
    //Se puede dar el caso que por ejemplo utilicemos el tamaño 16 y si no hicieramos esta comprobacion podria 
    //suceder que reservemos memoria para un tamaño que sea potencia de 2 lo que implicaria que el tamaño logico
    //  y fisico seria igual, y en el momento de aumentar el vector con una insercion conllevaria crear un nuevo buffer
    // perdiendo eficiencia   
}

/**
 * @brief Metodo para comprobar si hay que disminuir el tamaño fisico del vector debido al borrado de los objetos
 */
template <class T>
void VDinamico<T>::disminuye() {
    if (tamL * 3 < tamF) { //Si el tamL es < de 1/3 del tamF. tamF lo reducimos a la mitad
        tamF = tamF / 2;
        T *vaux = new T[tamF];
        for (unsigned int i = 0; i < tamL; i++)
            vaux[i] = v[i];
        delete []v;
        v = vaux;
    }
}



#endif /* VDINAMICO_H */

