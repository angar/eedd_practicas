/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Producto.h
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */


#ifndef PRODUCTO_H
#define PRODUCTO_H
//#include <iostream> //Pa
#include <string>   //std::string
#include <sstream> //std::stringstream para el CSV
#include<stdexcept> //std::domain_error

class Producto {
public:


    Producto(std::string cod = "", std::string desc = "", float _pvp = 0) throw (std::domain_error);
    Producto(const Producto& orig);
    virtual ~Producto();

    Producto& operator=(const Producto& right);

    void fromCSV(std::string& linea);
    std::string toCSV();

    void setPvp(float pvp) throw (std::domain_error);
    float getPvp() const;
    void setDescripcion(std::string descripcion);
    std::string getDescripcion() const;
    void setCodigo(std::string codigo);
    std::string getCodigo() const;


private:
    std::string codigo; ///< Codigo del producto
    std::string descripcion; ///< Descripcion detallada del producto
    float pvp; ///< Precio


};

#endif /* PRODUCTO_H */

