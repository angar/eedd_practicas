/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */

//#include <cstdlib>
#include "Producto.h"


#include "VDinamico.h"


#include <iostream>
//#include <climits>
#include <fstream> //std::ifstream (lectura fichero)


#include <cstring>

using namespace std;

void leecturafichero(VDinamico<Producto>& vec, std::string nomfichero) {

    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea
    fe.open(nomfichero.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                vec.insertar(lineaProd); //INsertamos en el vector dinamico el producto
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}

void mostrarVDinamico(VDinamico<Producto>& vec) {
    for (unsigned int i = 0; i < vec.tam(); i++)
        cout << i << ": " << vec[i].toCSV() << endl;
}

int main(int argc, char** argv) {


    std::setlocale(LC_ALL, "es_ES.UTF8");
    cout << "/***********************************************/" << endl;
    cout << "/**        PRACTICA 1 VECTOR DINAMICO       ****/" << endl;
    cout << "/***********************************************/" << endl;
    /**************************************************************************/
    /***                     APARTADO 1º                                    ***/
    /**************************************************************************/
    /*Instanciar el vector con todos los objetos de tipo Producto leídos desde el fichero en formato
    csv proporcionado */

    try {
        VDinamico<Producto> farmacia;

        string nomFich = "pharma1.csv"; //Si el archivo es el original eliminar la primera linea del archivo
        leecturafichero(farmacia, nomFich);

        int valteclado;
        cout << "\n 1)  Numero de productos en el vector Dinamico:  " << farmacia.tam();
        cout << "\n QUiere ver todos los productos (pulse 1), otro numero continuar: ";
        cin>>valteclado;
        if (valteclado == 1)
            mostrarVDinamico(farmacia);


        /**************************************************************************/
        /***                    FIN APARTADO 1º                                 ***/
        /**************************************************************************/






        /**************************************************************************/
        /***                     APARTADO 2º                                    ***/
        /**************************************************************************/
        /*Obtener otro vector con todos los medicamentos que sean tipo “crema” ó “CREMA”. */

        /*OBSERVACION:
         El metodo find de un string busca la primera ocurrencia en el string del string especificado en el argumento.
         Devuelve la poscion en la que se encuentra el primer caracter, sino lo encuentra devuelve el valor constante npos
         * es de tipo size_t y su valor es -1 */

        VDinamico<Producto> farmacia2;
        string CREMA = "CREMA";
        string crema = "crema";
        for (unsigned int i = 0; i < farmacia.tam(); i++) {
            if (farmacia[i].getDescripcion().find("CREMA") != std::string::npos || farmacia[i].getDescripcion().find("crema") != std::string::npos)
                farmacia2.insertar(farmacia[i]);
        }

        cout << "\n 2) NUmero de medicamentos que son de tipo 'crema' o 'CREMA': " << farmacia2.tam();

        cout << "\n QUiere ver todos los productos (pulse 1), otro numero continuar: ";
        cin>>valteclado;
        if (valteclado == 1)
            mostrarVDinamico(farmacia2);

        /**************************************************************************/
        /***                    FIN APARTADO 2º                                 ***/
        /**************************************************************************/



        /**************************************************************************/
        /***                     APARTADO 3º                                    ***/
        /**************************************************************************/
        /*Hacer una copia del original y mantener en la copia todos los medicamentos que cuesten entre
        5 y 15 euros.*/
        /*OBSERVACION:
         Para que funcione correctamente tenemos que empezar a eliminar del final al principio.
         Borrar significa sobrescribir posiciones. Si lo hicieramos desde el principio, 
         * pongamos un ejemplo: si queremos borrar la posicion cero lo que hace el vector Dinamico es poner el dato de
         * la posicion 1 en la 0 por lo tanto cada vez que se produciera un borrado el dato siguiente no lo comprobariamos
         */

        VDinamico<Producto> farmaciapre_5_15 = farmacia; //Copia del original
        //cout<<"\n Numero de medicamentos: "<<farmaciapre_5_15.tam();

        //for ( int i = 0; i < farmaciapre_5_15.tam(); i++)
        for (int i = farmaciapre_5_15.tam() - 1; i >= 0; i--) {//empezamos del final al principio de tamL-1 a 0

            if (farmaciapre_5_15[i].getPvp() < 5.0 || farmaciapre_5_15[i].getPvp() > 15.0)
                farmaciapre_5_15.borrar(i);

        }

        cout << "\n 3) NUmero de medicamentos que cuestan entre 5 y 15: " << farmaciapre_5_15.tam();

        cout << "\n QUiere ver todos los productos (pulse 1), otro numero continuar: ";
        cin>>valteclado;
        if (valteclado == 1)
            mostrarVDinamico(farmaciapre_5_15);
        /**************************************************************************/
        /***                    FIN APARTADO 3º                                 ***/
        /**************************************************************************/
        
        
    } catch (std::bad_alloc &e) {
        std::cerr << "Fallo reserva memoria " << e.what() << endl;
        throw e;

    } catch (std::invalid_argument &e) {
        std::cerr << e.what() << endl;

    } catch (std::exception &e) {
        std::cerr << e.what() << endl;
    }

    /*Otra posible solucion al  apartado anterior pero no es muy acertada es la siguiente
     El problema que tiene es que el operador >> vuelca del buffer hasta los separadores de espacio
     * y puede darse el caso que crema aparezca con una coma pegada o con otras vocales, por lo tanto la manera
     * de resolverlo no seria la correcta.
     */

    //    string pal;
    //    VDinamico<Producto> farmacia2;
    //    for (int i = 0; i < farmacia.tam(); i++) {
    //        std::stringstream aux(farmacia[i].getDescripcion());
    //        while (!aux.eof()) {
    //            aux >> pal;
    //            if (pal == CREMA || pal == crema) {
    //                farmacia2.insertar(farmacia[i]);
    //                break;
    //            }
    //        }
    //    }




    return 0;
}

