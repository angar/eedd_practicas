/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pedido.h
 * Author: usuario
 *
 * Created on 22 de octubre de 2018, 22:16
 */

#ifndef PEDIDO_H
#define PEDIDO_H

#include<string>
#include"VDinamico.h"
#include"Cliente.h"
#include"Producto.h"
#include<stdexcept> //std::invalid_argument

class Pedido {
public:
    
    enum ESTADO{PAGADO, ENALMACEN, ENTRANSITO, ENTREGADO} ;//Diferentes estados que puede tener un pedido

    Pedido(Cliente *cli) throw (std::invalid_argument);
    Pedido(const Pedido& orig);
    virtual ~Pedido();

    
    Pedido& operator=(const Pedido &right);
    
    void nuevoProducto(Producto *prod);
    float importe();
    int numProductos();
    Producto& getProducto(int i);
    Cliente* getCliente() const;

private:
    std::string id; ///< Identificador del pedido
    Cliente *cliente;///< Puntero a cliente, almacena un cliente (a traves de este atributo se establece la asociacion)
    ESTADO estado;///<  Estado del pedido {PAGADO, ENALMACEN, ENTRANSITO, ENTREGADO}
    VDinamico<Producto *> cesta; //Vector Dinamico de tipo Producto*, establece la asociacion 

};

#endif /* PEDIDO_H */

