/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliente.h
 * Author: usuario
 *
 * Created on 22 de octubre de 2018, 22:05
 */

#ifndef CLIENTE_H
#define CLIENTE_H

#include <string>
#include <sstream> //std::stringstream para el CSV

class Cliente {
public:
    
    Cliente(std::string _dni = "", std::string _nombre = "", std::string _pass = "", std::string _direccion = "");
    Cliente(const Cliente& orig);
    virtual ~Cliente();
    Cliente& operator=(const Cliente &right);
    void SetDireccion(std::string direccion);
    std::string GetDireccion() const;
    void SetPass(std::string pass);
    std::string GetPass() const;
    void SetNombre(std::string nombre);
    std::string GetNombre() const;
    void SetDni(std::string dni);
    std::string GetDni() const;
    
    std::string toCSV();

    
    
    
private:
    std::string dni;///< dni del cliente
    std::string nombre;///< nombre completo del cliente
    std::string pass;///< contraseña del cliente
    std::string direccion;///< direccion completa del cliente

};

#endif /* CLIENTE_H */

