/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliente.cpp
 * Author: usuario
 * 
 * Created on 22 de octubre de 2018, 22:05
 */

#include "Cliente.h"

/**
 * @brief Constructor por defecto parametrizado
 * Sino se asigna a ningun campo los campos tendran la cadena de caracteres vacia
 * 
 * @param _dni Dni del cliente
 * @param _nombre Nombre del cliente
 * @param _pass Contraseña del cliente
 * @param _direccion Direccion del cliente
 */
Cliente::Cliente(std::string _dni ,std::string _nombre, std::string _pass, std::string _direccion )
    : dni(_dni),
    nombre(_nombre),
    pass(_pass),
    direccion(_direccion){
}

/**
 * @brief Constructor copia
 * @param orig
 */
Cliente::Cliente(const Cliente& orig)
    :dni(orig.dni),
    nombre(orig.nombre),
    pass(orig.pass),
    direccion(orig.direccion){
}

/**
 * @brief Operador de asignacion 
 * @param right
 * @return 
 */
Cliente& Cliente::operator =(const Cliente& right){
    if(this != &right){
        dni=right.dni;
        nombre=right.nombre;
        pass=right.pass;
        direccion=right.direccion;
    }
    return *this;
}

void Cliente::SetDireccion(std::string direccion) {
    this->direccion = direccion;
}

std::string Cliente::GetDireccion() const {
    return direccion;
}

void Cliente::SetPass(std::string pass) {
    this->pass = pass;
}

std::string Cliente::GetPass() const {
    return pass;
}

void Cliente::SetNombre(std::string nombre) {
    this->nombre = nombre;
}

std::string Cliente::GetNombre() const {
    return nombre;
}

void Cliente::SetDni(std::string dni) {
    this->dni = dni;
}

std::string Cliente::GetDni() const {
    return dni;
}

Cliente::~Cliente() {
}

/**
 * @brief Representacion CVS de un cliente
 * @return std::string 
 */
std::string Cliente::toCSV(){
    std::stringstream aux;
    
    aux<< dni   << ";"
       << nombre<< ";"
       << pass  << ";"
       <<direccion<< ";";
    
    
    return aux.str();
}