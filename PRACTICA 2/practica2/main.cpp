/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */

//#include <cstdlib>
#include "Producto.h"
#include"Cliente.h"
#include"Pedido.h"
#include "VDinamico.h"
#include "ListaEnlazada.h"


#include <iostream>
//#include <climits>
#include <fstream> //std::ifstream (lectura fichero)
#include <clocale>//std::setlocale
#include <cstring>
#include <stdexcept>
#include <exception>


using namespace std;

void leecturafichero(VDinamico<Producto>& vec, std::string nomfichero);
void leecturafichero(ListaEnlazada<Producto>& vec, std::string nomfichero);
void mostrarVDinamico(VDinamico<Producto>& vec) ;
void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l);
void visualizaPedido(Pedido &l);


int main(int argc, char** argv) {

    cout << "/***********************************************/" << endl;
    cout << "/**        PRACTICA 2 LISTA ENLAZADA        ****/" << endl;
    cout << "/***********************************************/" << endl<<endl;

    std::setlocale(LC_ALL, "es_ES.UTF8");

    try {
        /**************************************************************************/
        /***                     APARTADO 1º                                    ***/
        /**************************************************************************/
        /*Instanciar una lista con todos los objetos de tipo Producto leídos desde 
         * el fichero en formato csv proporcionado*/
        //int valteclado;
        
        ListaEnlazada<Producto> listProd;
        string nomFich = "pharma1.csv"; //Si el archivo es el original eliminar la primera linea del archivo
        leecturafichero(listProd, nomFich);
        cout << "Tamaño lista de Productos inicial (lisProd): " << listProd.tam() << endl;
       //mostrarListaEnlazadaProd(listProd);
        
        /**************************************************************************/
        /***                    FIN APARTADO 1º                                 ***/
        /**************************************************************************/



        /**************************************************************************/
        /***                     APARTADO 2º                                    ***/
        /**************************************************************************/
        /*Obtener una lista con todos los medicamentos que sean tipo “crema” ó “CREMA” y otra con
        los que contengan la subcadena “AGUA” */
        ListaEnlazada<Producto> listCrema;
        ListaEnlazada<Producto> listAgua;

        ListaEnlazada<Producto>::Iterador itelistProd = listProd.iterador();
        while (!itelistProd.fin()) {
            if (itelistProd.dato().getDescripcion().find("CREMA") != std::string::npos || itelistProd.dato().getDescripcion().find("crema") != std::string::npos)
                listCrema.insertaFin(itelistProd.dato());

            if (itelistProd.dato().getDescripcion().find("AGUA") != std::string::npos)
                listAgua.insertaFin(itelistProd.dato());

            itelistProd.siguiente();
        }

        cout << "Tamaño de lista Producto de tipo 'crema' o 'CREMA'(listCrema): " << listCrema.tam() << endl;
        cout << "Tamaño de lista Producto de tipo 'AGUA' (listAgua): " << listAgua.tam() << endl;
        //mostrarListaEnlazadaProd(listCrema);
        //mostrarListaEnlazadaProd(listAgua);
        
        /**************************************************************************/
        /***                    FIN APARTADO 2º                                 ***/
        /**************************************************************************/



        /**************************************************************************/
        /***                     APARTADO 3º                                    ***/
        /**************************************************************************/
        /*Obtener una lista con la concatenación de ambas sublistas y visualizar la lista obtenida*/
        ListaEnlazada<Producto> concaCremaAgua;
        concaCremaAgua = listCrema.concatena(listAgua);
        cout << "Tamaño lista concatenada (concaCreamaAgua): " << concaCremaAgua.tam() << endl;
        //mostrarListaEnlazadaProd(concaCremaAgua);
        /**************************************************************************/
        /***                    FIN APARTADO 3º                                 ***/
        /**************************************************************************/



        /**************************************************************************/
        /***                     APARTADO 4º                                    ***/
        /**************************************************************************/
        /**Crear un objeto ​ Cliente a modo de prueba y crear igualmente un objeto ​ Pedido con al menos
        20 productos enlazados siempre a la lista general. Los pedidos de ese cliente son todos de
        100ML.*/
        Cliente angel("999999", "angel garcia", "1234", "jaen");
        Pedido compra(&angel);

        //Inicimas otra vez el iterador
        itelistProd = listProd.iterador();

        while (compra.numProductos() != 20) {
            if (itelistProd.dato().getDescripcion().find("100ML") != std::string::npos) {
                compra.nuevoProducto(&(itelistProd.dato()));
                //            cout<<"direccion lista:"<<&itelistProd.dato()<<endl;
                //            cout<<"direccion vector"<<&compra.getProducto(i++);
            }
            itelistProd.siguiente();
        }
        cout << "Tamaño de productos del pedido (compra): " << compra.numProductos() << endl;
        
        /**************************************************************************/
        /***                    FIN APARTADO 4º                                 ***/
        /**************************************************************************/

        
        
        /**************************************************************************/
        /***                     APARTADO 5º                                    ***/
        /**************************************************************************/
        /*Crear una función ​ visualiza(Pedido)​ , en main, para mostrar todos los datos de un
        pedido: Información del cliente, detalles de los productos y precio total, y utilizarla para
        mostrar los datos del pedido creado.*/
        
        visualizaPedido(compra);
        /**************************************************************************/
        /***                    FIN APARTADO 5º                                 ***/
        /**************************************************************************/

    } catch (std::bad_alloc &e) {
        std::cerr << "Fallo reserva memoria " << e.what() << endl;
        throw e;

    } catch (std::invalid_argument &e) {
        std::cerr << e.what() << endl;
        throw e;

    } catch (std::exception &e) {
        std::cerr << e.what() << endl;
    }
    return 0;
}



/**
 * @brief Lectura de fichero para vector dinamico de tipo producto
 * @param vec
 * @param nomfichero
 */
void leecturafichero(VDinamico<Producto>& vec, std::string nomfichero) {

    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea
    fe.open(nomfichero.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                vec.insertar(lineaProd); //INsertamos en el vector dinamico el producto
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}

/**
 * @brief Funcion para lectura de fichero de una lista enlazada de tipo producto
 * @param vec
 * @param nomfichero
 */
void leecturafichero(ListaEnlazada<Producto>& vec, std::string nomfichero) {

    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea
    fe.open(nomfichero.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                vec.insertaFin(lineaProd); //INsertamos en la lista enlazada por el final
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}


void mostrarVDinamico(VDinamico<Producto>& vec) {
    for (unsigned int i = 0; i < vec.tam(); i++)
        cout <<"Producto "<< i << ": " << vec[i].toCSV() << endl;
}

void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l){
    ListaEnlazada<Producto>::Iterador iteListProd=l.iterador();
    int i=1;
    while (!iteListProd.fin()){
        cout<<"Producto "<<i++<<": "<<iteListProd.dato().toCSV()<<endl;
        iteListProd.siguiente();
    }
        
}


void visualizaPedido(Pedido &l) {
    cout << "/*************************/" << endl;
    cout << "/*   Datos del pedido   **/" << endl;
    cout << "/*************************/" << endl;
    cout << endl;
    cout << "/***Datos Cliente  *******/" << endl;
    cout << l.getCliente()->toCSV()<<endl;
//    cout << "DNI: " << l.getCliente()->GetDni() << endl;
//    cout << "Nombre: " << l.getCliente()->GetNombre() << endl;
//    cout << "Pass: " << "*******" << endl;
//    cout << "Direccion: " << l.getCliente()->GetDireccion() << endl;
    cout << endl;
    cout << "/***Detalles de productos *******/" << endl;
    for (int i = 0; i < l.numProductos(); i++) {
        cout << l.getProducto(i).toCSV() << endl;
    }
    cout << endl;
    cout << "/***Precio total compra *******/" << endl;
    cout << l.importe() << " €";
}