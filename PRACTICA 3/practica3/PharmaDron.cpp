/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PharmaDron.cpp
 * Author: usuario
 * 
 * Created on 8 de noviembre de 2018, 11:28
 */

#include "PharmaDron.h"

/**
 * @brief Constructor por defecto
 */
PharmaDron::PharmaDron()
    :productos(),
    clientes(),
    posicion(){
}
/**
 * COnstructo copia
 * @param orig
 */
PharmaDron::PharmaDron(const PharmaDron& orig)
    :productos(orig.productos),
    clientes(orig.clientes),
    posicion(orig.posicion){
}

PharmaDron& PharmaDron::operator=(const PharmaDron right) {
    if(this !=&right){
        productos=right.productos;
        clientes=right.clientes;
        posicion=right.posicion;
    }
    return *this;
}


/**
 * Desctructor
 */
PharmaDron::~PharmaDron() {
}

/**
 * @brief Metodo para cargar los productos en la esctructura de datos de tipo ListaEnalzada 
 * @param fileNameProductos Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::cargaProductos(std::string& fileNameProductos){
    
    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea, le asignamos nuestro PharmaDron
    fe.open(fileNameProductos.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                productos.insertaFin(lineaProd); //INsertamos en la lista enlazada por el final
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}

/**
 * @brief Metodo para cargar los clientes en la esctructura de datos de tipo AVL 
 * le asignamos a cada cliente el PharmaDron (this)
 * @param fileNameClientes Nombre del archivo del cual cargaremos los datos
 * @pre Se necesita tener implementado el metodo fromCSV
 */
void PharmaDron::crearClientes (std::string &fileNameClientes){
    std::ifstream fe;
    std::string linea;

    Cliente lineaCli(this); //Creamos un cliente para asignar el CSV linea a linea, , le asignamos nuestro PharmaDron
    fe.open(fileNameClientes.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaCli.fromCSV(linea); //Utilizamos el metodo CSV de clientes para rellenar sus atributos
                clientes.insertar(lineaCli);//INsertamos en el AVL
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }    
}

/**
 * @brief Metodo para añadir un nuevo prodcuto a la lista Enlazada de productos
 * Lo pasamos por referencia porque el producto no esta en el sistema
 * @param p
 */
void PharmaDron::aniadeProducto(Producto& p) {
    productos.insertaFin(p);
    
}


/**
 * @brief Metodo para añadir un nuevo cliente al AVL de clientes
 * No tiene que ser un puntero porque añado el cliente al sistema
 * @param c
 */
void PharmaDron::nuevoCliente(Cliente& c) {
    clientes.insertar(c);
}

/**
 * @brief Metodo que busca un Producto en el PharmaDron si coincide con la subcadena
 * @param subcadena
 * @return ListaEnlazada<Producto*> necesito que sean punteros para tenerlos enganchados del sistema
 * @post Si subcadena es la cadena vacia, devuelve una listaEnlazada vacia
 */
ListaEnlazada<Producto*> PharmaDron::buscaProducto(std::string& subcadena) {
    ListaEnlazada<Producto*> listaProduc;
    //VDinamico<Producto *> vecProduc;//Tambien podria devolverse porque para insertar y recorrer es el mismo O que lista
    if (subcadena != "") {
        ListaEnlazada<Producto>::Iterador iteLisProd = productos.iterador();
        while (!iteLisProd.fin()) {
            if (iteLisProd.dato().getDescripcion().find(subcadena) != std::string::npos)
                listaProduc.insertaFin(&iteLisProd.dato());
            iteLisProd.siguiente();
        }
    }
    return listaProduc;
}

/**
 * @brief Metodo para Ingresar un cliente, dado su dni y su pass
 * Si no se da el pass solo comprueba el dni
 * @param dni
 * @param pass
 * @return CLiente* puntero al cliente del sistema buscado 
 */
Cliente* PharmaDron::ingresaCliente(std::string dni, std::string pass) {
    Cliente buscado(dni,pass);
    Cliente *encontrado = 0;
    if (pass == "") {//Si no se no se nos da pass
        if (clientes.buscar(buscado, encontrado))
            return encontrado;
    } else {//Si se da el pass
        if (clientes.buscar(buscado, encontrado)) 
            if (encontrado->GetPass() == buscado.GetPass()) 
                return encontrado;
    }
    return 0;
}

/**
 * @brief Metodo dado un cliente nos da su pedido si el cliente esta en el sistema
 * @param c
 * @return 
 * @throws std::invalid_argument si no encuentra el cliente
 */
Pedido& PharmaDron::verPedido(Cliente& c) {
    
    Cliente *busc=this->ingresaCliente(c.GetDni(),c.GetPass());
    if (busc!=0)
        return busc->getPedidos();
    else
        throw std::invalid_argument("[PharmaDron::verPedido]: el cliente no exite, no se puede ver sus pedidos");
}

/**
 * @brief Metodo para saber el numero de cliente en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numClientes() {
    
    //std::cout<<"algura"<<clientes.altura()<<std::endl;
    return clientes.numElementos();
}


/**
 * @brief Metodo para visualizar los clientes en la estructura AVL, recorrido inorden.
 */
void PharmaDron::visualizarClientes() {
    clientes.recorreInorden();
}

/**
 * @brief Metodo para saber el numero de productos en nuestro PharmaDron
 * @return 
 */
int PharmaDron::numProductos() {
    return productos.tam();
}


