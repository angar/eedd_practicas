/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: admin
 *
 * Created on 21 de septiembre de 2018, 9:44
 */

//#include <cstdlib>
#include "Producto.h"
#include"Cliente.h"
#include"Pedido.h"
#include "VDinamico.h"
#include "ListaEnlazada.h"
#include "AVL.h"
#include "PharmaDron.h"


#include <iostream>
//#include <climits>
#include <fstream> //std::ifstream (lectura fichero)
#include <clocale>//std::setlocale
#include <cstring>
#include <stdexcept>
#include <exception>


using namespace std;

//Practica 1
void leecturafichero(VDinamico<Producto>& vec, std::string nomfichero);
void mostrarVDinamico(VDinamico<Producto>& vec) ;

//Practica 2
void leecturafichero(ListaEnlazada<Producto>& vec, std::string nomfichero);
void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l);
void visualizaPedido(Pedido &l);


//Practica 3
void pruebaArbol(void);

int main(int argc, char** argv) {

    cout << "/***********************************************/" << endl;
    cout << "/****        PRACTICA 3 ARBOLES AVL         ****/" << endl;
    cout << "/***********************************************/" << endl << endl;

    std::setlocale(LC_ALL, "es_ES.UTF8");
    try {
        /**************************************************************************/
        /***                     APARTADO 1º                                    ***/
        /**************************************************************************/
        //pruebaArbol();


        /**************************************************************************/
        /***                     APARTADO 2º                                    ***/
        /**************************************************************************/
        PharmaDron gestion;

        string nomFichProduc = "pharma1.csv";
        string nomFichCLien = "clientes.csv";
        gestion.cargaProductos(nomFichProduc);
        gestion.crearClientes(nomFichCLien);
        //gestion.buscaProducto(nomFichProduc);
        //gestion.cargaProductos()
        cout << "TAMAÑO DE clientes en PharmaDron:" << gestion.numClientes() << endl;
        cout << "TAMAÑO DE productos en PharmaDron:" << gestion.numProductos() << endl << endl;
        //gestion.visualizarClientes();
        string dni = "43954678C"; //valido posicion en csv 1655 (nombre: Roderic Wickman)con la primera linea de informacion eliminada
        string _100ML = "100ML";
        string crema = "CREMA";
        ListaEnlazada<Producto *> listaProduct1;
        ListaEnlazada<Producto *> listaProduct2;
        ListaEnlazada<Producto *> listaProduct3;
        ListaEnlazada<Producto *>::Iterador iteLisProdPunteros;

        Cliente *buscado = 0;
        buscado = gestion.ingresaCliente(dni);

        //INSERTAMOS LOS PRODUCTOS DE 100 ML AL CLIENTE RODERIC
        //auto i=buscado->buscarProducto(_100ML);
        if (buscado != 0) {
            listaProduct1 = buscado->buscarProducto(_100ML);
            //cout<<"TAM lista de 100 ml: "<<listaProduct.tam()<<endl;

            iteLisProdPunteros = listaProduct1.iterador();

            while (!iteLisProdPunteros.fin()) {
                buscado->addProductoPedido(iteLisProdPunteros.dato());
                iteLisProdPunteros.siguiente();
            }
            cout << endl << "*****CLIENTE 1******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }



        //INSERTAMOS LOS PRODUCTOS DE CREMA AL CLIENTE 34923452L Abbi Kirvin POSICION CSV 10000
        dni = "34923452L";
        buscado = gestion.ingresaCliente(dni);


        if (buscado != 0) {
            listaProduct2 = buscado->buscarProducto(crema);
            //cout<<"TAM lista de crema: "<<listaProduct.tam()<<endl;

            iteLisProdPunteros = listaProduct2.iterador();

            while (!iteLisProdPunteros.fin()) {
                buscado->addProductoPedido(iteLisProdPunteros.dato());
                iteLisProdPunteros.siguiente();
            }
            cout << endl << "*****CLIENTE 2******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }



        //INSERTAMOS LOS PRODUCTOS DE CREMA o 100ML AL CLIENTE 10982609X Marlie Klassmann POSICION CSV 1
        dni = "10982609X";
        buscado = gestion.ingresaCliente(dni);


        if (buscado != 0) {
            listaProduct3 = listaProduct1.concatena(listaProduct2);
            //cout<<"TAM lista de: "<<listaProduct3.tam()<<endl;

            iteLisProdPunteros = listaProduct3.iterador();

            while (!iteLisProdPunteros.fin()) {
                buscado->addProductoPedido(iteLisProdPunteros.dato());
                iteLisProdPunteros.siguiente();
            }
            cout << endl << "*****CLIENTE 3******" << endl;
            cout << buscado->toCSV() << endl;
            cout << "Numero de productos pedido: " << buscado->getPedidos().numProductos() << endl;
            cout << "Precio total pedido: " << buscado->getPedidos().importe() << endl;
            //visualizaPedido(buscado->getPedidos());
        }

    } catch (std::bad_alloc &e) {
        std::cerr << "Fallo reserva memoria " << e.what() << endl;
        throw e;

    } catch (std::invalid_argument &e) {
        std::cerr << e.what() << endl;
        throw e;

    } catch (std::exception &e) {
        std::cerr << e.what() << endl;
    }
    return 0;
}



/**
 * @brief Lectura de fichero para vector dinamico de tipo producto
 * @param vec
 * @param nomfichero
 */
void leecturafichero(VDinamico<Producto>& vec, std::string nomfichero) {

    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea
    fe.open(nomfichero.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                vec.insertar(lineaProd); //INsertamos en el vector dinamico el producto
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}

/**
 * @brief Funcion para lectura de fichero de una lista enlazada de tipo producto
 * @param vec
 * @param nomfichero
 */
void leecturafichero(ListaEnlazada<Producto>& vec, std::string nomfichero) {

    std::ifstream fe;
    std::string linea;

    Producto lineaProd; //Creamos un producto para asignar el CSV linea a linea
    fe.open(nomfichero.c_str());

    if (fe.good()) { //Si la apertura es correcta empezamos la lectura del archivo
        while (!fe.eof()) { //Mientras no se llegue al final del archivo
            getline(fe, linea);
            if (linea != "") { //Por si hay lineas vacias
                lineaProd.fromCSV(linea); //Utilizamos el metodo CSV de producto para rellenar sus atributos
                vec.insertaFin(lineaProd); //INsertamos en la lista enlazada por el final
            }
        }
        fe.close();
    } else { //Sino, hay fallo en la apertura del archivo
        std::cerr << "No se puede abrir el fichero" << std::endl;
    }
}


void mostrarVDinamico(VDinamico<Producto>& vec) {
    for (unsigned int i = 0; i < vec.tam(); i++)
        cout <<"Producto "<< i << ": " << vec[i].toCSV() << endl;
}

void mostrarListaEnlazadaProd (ListaEnlazada<Producto>& l){
    ListaEnlazada<Producto>::Iterador iteListProd=l.iterador();
    int i=1;
    while (!iteListProd.fin()){
        cout<<"Producto "<<i++<<": "<<iteListProd.dato().toCSV()<<endl;
        iteListProd.siguiente();
    }
        
}


void visualizaPedido(Pedido &l) {
    cout << "/*************************/" << endl;
    cout << "/*   Datos del pedido   **/" << endl;
    cout << "/*************************/" << endl;
    cout << endl;
    cout << "/***Datos Cliente  *******/" << endl;
    //cout << l.getCliente()->toCSV()<<endl;
//    cout << "DNI: " << l.getCliente()->GetDni() << endl;
//    cout << "Nombre: " << l.getCliente()->GetNombre() << endl;
//    cout << "Pass: " << "*******" << endl;
//    cout << "Direccion: " << l.getCliente()->GetDireccion() << endl;
    cout << endl;
    cout << "/***Detalles de productos *******/" << endl;
    for (int i = 0; i < l.numProductos(); i++) {
        cout << l.getProducto(i).toCSV() << endl;
    }
    cout << endl;
    cout << "/***Precio total compra *******/" << endl;
    cout << l.importe() << " €";
}


void pruebaArbol(void){
    
    /*Se declarará un AVL<int> en el que se introducirán un millón de números 
     * enteros consecutivos. Se retornará la altura del árbol, y se harán dos 
     * búsquedas, una con resultado positivo y otra de un número que no esté 
     * (usar ambos métodos de búsqueda).
     */
    AVL<int> arbolint;
    
    for(int i=0;i <1000000;i++){
        arbolint.insertar(i);   
    }
    cout<<"Altura arbol enteros   : "<<arbolint.altura()<<endl;
    cout<<"Elementos arbol enteros: "<<arbolint.numElementos()<<endl;
    //arbolint.recorreInorden();
    
    int eleBuscado=500;
    int *resultado=0;
    //Busqueda con elemento en el AVL
    cout<<endl<<"Busqueda Recursiva"<<endl;
    if (arbolint.buscar(eleBuscado,resultado))
        cout<<"Dato buscado ("<<eleBuscado<<"): Encontrado"<<endl;
    else
        cout<<"Dato buscado ("<<eleBuscado<<"): No encontrado"<<endl;
    
    cout<<endl<<"Busqueda Iterativa"<<endl;
    if (arbolint.buscarIt(eleBuscado,resultado))
        cout<<"Dato buscado ("<<eleBuscado<<"): Encontrado"<<endl;
    else
        cout<<"Dato buscado ("<<eleBuscado<<"): No encontrado"<<endl;
    
    //Busqueda sin elemento en el AVL
    eleBuscado=2000000;
    cout<<endl<<"Busqueda Recursiva"<<endl;
    if (arbolint.buscar(eleBuscado,resultado))
        cout<<"Dato buscado ("<<eleBuscado<<"): Encontrado"<<endl;
    else
        cout<<"Dato buscado ("<<eleBuscado<<"): No encontrado"<<endl;
    
    cout<<endl<<"Busqueda Iterativa"<<endl;
    if (arbolint.buscarIt(eleBuscado,resultado))
        cout<<"Dato buscado ("<<eleBuscado<<"): Encontrado"<<endl;
    else
        cout<<"Dato buscado ("<<eleBuscado<<"): No encontrado"<<endl;    
    
}