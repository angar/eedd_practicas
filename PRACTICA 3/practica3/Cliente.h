/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliente.h
 * Author: usuario
 *
 * Created on 22 de octubre de 2018, 22:05
 */

#ifndef CLIENTE_H
#define CLIENTE_H

#include <string> //std::string, std::stof
#include <sstream> //std::stringstream para el CSV
#include <ostream> //std::ostream operator <<

#include "UTM.h"
#include "Pedido.h"
#include "ListaEnlazada.h"
#include "PharmaDron.h"

class PharmaDron;

class Cliente {
public:
    
    Cliente(std::string _dni = "", std::string _nombre = "", std::string _pass = "", std::string _direccion = "",
            float latit=0, float longi=0, PharmaDron *phar=0);
    Cliente(PharmaDron *phar);
    Cliente(const Cliente& orig);
    virtual ~Cliente();
    Cliente& operator=(const Cliente &right);
    
    void SetDireccion(std::string direccion);
    std::string GetDireccion() const;
    void SetPass(std::string pass);
    std::string GetPass() const;
    void SetNombre(std::string nombre);
    std::string GetNombre() const;
    void SetDni(std::string dni);
    std::string GetDni() const;
    
    std::string toCSV() const;
    void fromCSV(std::string &linea);

    ListaEnlazada<Producto *> buscarProducto (std::string &subcadena);
    void addProductoPedido(Producto *p);
    
    Pedido& getPedidos();
    
    bool operator <(const Cliente & right) const;
    bool operator >(const Cliente & right) const;
    bool operator ==(const Cliente & right) const;
    bool operator <=(const Cliente & right) const;
    
//    friend std::ostream& operator<<(std::ostream& out, const Cliente& c) {
//        return out << c.toCSV();
//    }

private:
    std::string dni;///< dni del cliente
    std::string nombre;///< nombre completo del cliente
    std::string pass;///< contraseña del cliente
    std::string direccion;///< direccion completa del cliente
    UTM posicion;///< posicion de la direccion del cliente (coordenadas)
    PharmaDron *pharma;///< Asociacion con PharmaDron
    Pedido pedidos;///< Composicion con la clase pedido    
                    //Se podria hacer con puntero pero tendriamos que reservar memoria y liberar    
};


    std::ostream& operator<<(std::ostream& out, const Cliente& c); 


#endif /* CLIENTE_H */

