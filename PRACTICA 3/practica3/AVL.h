/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AVL.h
 * Author: usuario
 *
 * Created on 26 de octubre de 2018, 9:34
 */

#ifndef AVL_H
#define AVL_H

//#include <string>
#include <iostream>


template <typename T>
class AVL {
    
private:
    class Nodo {
    public://Los atributos tiene que ser publicos para acceder desde AVL
        Nodo *izq;
        Nodo *der;
        T dato;
        char bal; ///-1,0, 1 para el arbol AVL
    public:
        Nodo();
        Nodo(T &ele, char balan = 0);
        Nodo(const Nodo &orig);
        Nodo& operator=(const Nodo right);
    };

public:
    AVL();
    AVL(const AVL& orig);
    virtual ~AVL();
    AVL& operator=(const AVL &right);

    
public:
    void recorrePreorden();
    void recorreInorden();
    void recorrePostorden();
    bool insertar(T &ele);
    bool buscar(T &ele, T* &result);
    unsigned int numElementos();
    unsigned int altura();
    bool buscarIt(T &ele, T* &result);

private:
    void preorden(Nodo *p, int nivel);
    void inorden(Nodo *p, int nivel);
    void postorden(Nodo *p, int nivel);
    Nodo * buscaClave(T &ele, Nodo *p);
    void preordenNumEle(Nodo *p, unsigned int &num);
    void preordenAl(Nodo *p, int nivel, unsigned int &altura);
    void borrarArbol(Nodo * &p);
    void copiaPreorden(Nodo* &this_, Nodo* right_);
    Nodo * buscaClaveIt(T &ele, Nodo *p);
    int inserta(Nodo* &c, T &dato);
    void rotDecha(Nodo* &p);
    void rotIzqda(Nodo* &p);


private:
    Nodo *raiz;

};



/**************************************************************************/
/***                    NODO  AVL                                       ***/
/**************************************************************************/

/**
 * @brief Constructor por defecto de nodo
 */
template <typename T>
AVL<T>::Nodo::Nodo()
    :izq(0),
    der(0),
    bal(0){
}

/**
 * @brief Constructor parametrizado con valores por defecto
 * Necesario para el operador de asigancion de AVL necesitamos copiar el balanceo
 * @param ele 
 * @param balan 
 */
template <typename T>
AVL<T>::Nodo::Nodo(T& ele,char balan)
    : izq(0),
    der(0),
    dato(ele),
    bal(balan){
}


/**
 * @brief Constructo copia de nodo
 * @param orig
 */
template <typename T>
AVL<T>::Nodo::Nodo(const Nodo &orig)
    :izq(orig.izq),
    der(orig.der),
    dato(orig.dato){
}

/**
 * @brief OPerador de asignacion de nodo
 * @param right
 * @return 
 */
template <typename T>
typename AVL<T>::Nodo& AVL<T>::Nodo::operator =(const Nodo right){
    if (this!= &right){
        izq=right.izq;
        der=right.der;
        dato=right.dato;
        bal=right.bal;
    }
    return *this;
}

/**************************************************************************/
/***                    FIN NODO  AVL                                   ***/
/**************************************************************************/



//
//AVL<T>::AVL(const AVL& orig){
//    necesito metodo que pase la raiz y de alguna manera cree una copia de esa raiz
//    copiaNodos(orig.raiz, this->raiz);//funcion recursiva
//}
//
//AVL<T>::_copiaNodos(Nodo * rorig, Nodo *&rdest){
//    rdest=new(rorig);
//    //repita proceso hijo iz y der
//    
//}


/**************************************************************************/
/***                          AVL                                       ***/
/**************************************************************************/

/**
 * @brief Constructor por defecto
 */
template <typename T>
AVL<T>::AVL()
    :raiz(0){
}

/**
 * @brief Constructor copia de AVL
 * Utiliza el metodo privado copiaPreorden
 * @param orig 
 */
template <typename T>
AVL<T>::AVL(const AVL& orig)
    :raiz(0){
    copiaPreorden(raiz, orig.raiz);
}

/**
 * @brief Metodo privado recursivo para copiar un AVL, se necesita el recorrido Preorden(padre,izq,der)
 * Si el arbol del que vamos a copiar estuviera vacio funciona correctamente
 * @param this_ Puntero referenciado para poder modificar los punteros del arbol this
 * @param right_ Puntero a nodo del objeto AVL que se va a copiar
 */
template <typename T>
void AVL<T>::copiaPreorden(Nodo*& this_, Nodo* right_){//this tiene que ser referenciado para hacer el new y que se modifique el hijo izq y derecha
    if (right_){
        this_=new Nodo(right_->dato,right_->bal);//TODO hacer que el nodo para avl copie el balanceo
        copiaPreorden(this_->izq,right_->izq);
        copiaPreorden(this_->der,right_->der);
    }else{//si esta vacio inicializamos a cero
        this_=nullptr;
    }
}

/**
 * @brief Operador de asignacion de AVL
 * Primero borramos el arbol, despues hacemos una copia de los datos y balanceo identica
 * @param right
 * @return 
 */
template <typename T>
AVL<T>& AVL<T>::operator =(const AVL& right){
    if (this != &right){
        borrarArbol(this->raiz);
        copiaPreorden(this->raiz,right.raiz);
    }
    return *this;
}

/**
 * @brief Destructor de AVL
 * Utiliza el metodo borrarArbol
 */
template <typename T>
AVL<T>::~AVL(){
    borrarArbol(raiz);
}


/**
 * @brief Metodo privado que Libera el arbol de forma recursiva, 
 * Utilizando el recorrido en postorden(izq,der,padre)
 * @param p
 */
template <typename T>
void AVL<T>::borrarArbol(Nodo* &p){//para que al hacer p=0 se necesita una referencia al puntero
        if (p){
        borrarArbol(p->izq);    //izq
        borrarArbol(p->der);    //der
        delete p;               //padre
        p=0;
        }
}


/**
 * @brief Metodo privado recursivo, para recorrido PREORDEN
 * @param p 
 * @param nivel
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::preorden(Nodo* p, int nivel){
    if(p){
        //
        std::cout<<"procesando nodo"<<p->dato;
        std::cout<< "en el nivel"<<nivel<<std::endl;
        //
        preorden(p->izq, nivel+1);
        preorden(p->der,nivel+1);
    }
}


/**
 * @brief Metodo privado recursivo, para recorrido INORDEN
 * @param p 
 * @param nivel
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::inorden(Nodo* p, int nivel){
    if (p){
        //
        inorden(p->izq,nivel+1);
        std::cout<< "nodo: "<<p->dato/*<<" bal: "<<(int)p->bal*/;
        std::cout<< "   en el nivel " <<nivel<<std::endl;

        inorden(p->der,nivel+1);
    }
}

/**
 * @brief Metodo privado recursivo, para recorrido POSORDEN
 * @param p 
 * @param nivel
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::postorden(Nodo* p, int nivel){
    if (p){
        //
        postorden(p->izq,nivel+1);
        postorden(p->der,nivel+1);
        std::cout<< "Procesando nodo: "<<p->dato;
        std::cout<< " en el nivel" <<nivel<<std::endl;


    }
}

/**
 * @brief Metodo para recorrido PREORDEN
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::recorrePreorden() {
    preorden(raiz,0);
}

/**
 * @brief Metodo para recorrido INORDEN
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::recorreInorden(){
    inorden(raiz,0);
}

/**
 * @brief Metodo para recorrido POSTORDEN
 * @pre Si el T es una clase debe tener implementado el operador << 
 */
template <typename T>
void AVL<T>::recorrePostorden(){
    postorden(raiz,0);
}


/**
 * @brief Metodo privado, que busca el objeto de tipo T en el AVL
 * Este metodo es necesario para el metodo buscar
 * @param ele
 * @param p
 * @return 
 * @pre Si el T es una clase debe tener implementado el operador < y >
 */
template <typename T>
typename AVL<T>::Nodo* AVL<T>::buscaClave(T& ele, Nodo* p) {
    if (!p)
        return 0;
    else if (ele < p->dato)
        return buscaClave(ele,p->izq);
    else if (ele > p->dato)
        return buscaClave(ele,p->der);
    else return p;
}


/**
 * @brief Metodo que busca un objeto ele de tipo T, si lo encuentra lo devolvera en resutl
 * @param ele Elemento que buscamos
 * @param result
 * @return true si lo ha encontrado, false si no esta en el arbol
 * @pre Si el T es una clase debe tener implementado el operador < y >
 * tambien se utiliza el operador =
 */
template <typename T>
bool AVL<T>::buscar(T& ele, T* &result){
    Nodo *p= buscaClave (ele,raiz);
    if (p){
        result=&(p->dato);
        return true;
    }
    return false;
}

/**
 * @brief MEtodo para insertar un dato en el AVL
 * @param ele
 * @return false, si no ha insertardo, true, si lo ha insertado
 * @pre Si el T es una clase debe tener implementado el operador < y >
 * tambien se utiliza el operador =
 */
template <typename T>
bool AVL<T>::insertar(T& ele){
    //T res;//TODO ESTO FALLARA antes res era una referencia pero es mejor puntero porque quiero saber si lo encuentra o no
    //sino lo encuentra res sera cero, si lo encuentra tendra una direccion de memoria
    T* res=0;
    
    bool encontrado = buscar (ele,res);
    if(!encontrado){
        inserta(raiz,ele);
        return true;
    }
    return false;
}


/**
 * Metodo privado recursivo, que inserta en el AVL
 * @param c
 * @param dato
 * @return 
 */
template <typename T>
int AVL<T>::inserta(Nodo*& c, T& dato){
    Nodo *p=c;
    int deltaH=0;
    if(!p){
        p =new Nodo(dato);
        c=p; deltaH=1;
    }
    else if(dato> p->dato){
        if (inserta(p->der,dato)){
            p->bal--;
            if(p->bal==-1) deltaH=1;
            else if (p->bal ==-2){
                if (p->der->bal ==1) rotDecha(p->der);
                rotIzqda(c);
            }
        }
    }
    else if(dato<p->dato){
        if (inserta(p->izq,dato)){
            p->bal++;
            if(p->bal==1)deltaH =1;
            else if (p->bal ==2){
                if(p->izq->bal ==-1) rotIzqda (p->izq);
                rotDecha(c);
            }
        }
    }
    return deltaH;
    
}

/**
 * @brief Metodo para rotar un nodo desequilibrado a la izquierda
 * @param p puntero referenciado 
 */
template <typename T>
void AVL<T>::rotIzqda(Nodo*& p){
    Nodo *q =p, *r;
    p = r = q->der;
    q->der = r->izq;
    r->izq = q;
    q->bal++;
    if (r->bal < 0) q->bal += -r->bal;
    r->bal++;
    if(q->bal > 0) r->bal += q->bal;
}

/**
 * @brief Metodo para rotar un nodo desequilibrado a la derecha
 * @param p puntero referenciado 
 */
template <typename T>
void AVL<T>::rotDecha(Nodo*& p){
    Nodo *q = p, *l;
    p = l = q->izq;
    q->izq = l->der;
    l->der = q;
    q->bal--;
    if (l->bal > 0) q->bal -=l->bal;
    l->bal--;
    if(q->bal < 0) l->bal -= -q->bal;
    
}


/**
 * @brief Metodo para saber el numero de elementos que tenemos en el AVL
 * @return entero con los elementos
 */
template <typename T>
unsigned int AVL<T>::numElementos(){
    unsigned int contador=0;
    preordenNumEle(raiz, contador);
    return contador;
}


/**
 * MEtodo recursivo privado para calcular el numero de elementos en el AVL
 * Se ha recorrido en preorden pero cualquier recorrido de los tres es valido
 * para calcular el numero de elementos
 * @param p
 * @param num referencia a un entero sin signo para modifica en cada llamada recursiva
 */
template <typename T>
void AVL<T>::preordenNumEle(Nodo *p, unsigned int &num){
    if (p){
        ++num;
        preordenNumEle(p->izq,num);
        preordenNumEle(p->der,num);
    }
}

/**
 * @brief Metodo que calcura la altura del arbol AVL
 * @return 
 */
template <typename T>
unsigned int AVL<T>::altura(){
    unsigned int alt=0;
    preordenAl(raiz,0,alt);
    return alt;
}

/**
 * Metodo privado recursivo, que calcula la altura del arbol AVL
 * @param p
 * @param nivel
 * @param altura
 */
template <typename T>
void AVL<T>::preordenAl(Nodo* p, int nivel, unsigned int& altura){//una referencia porque altura se tiene que guardar la modificacion si la altura ha cambiado
    if (p){
        if ((unsigned int)nivel>altura){
            altura=nivel;
        }
        preordenAl(p->izq, nivel+1,altura);
        preordenAl(p->der, nivel+1,altura);
    }
}



/**
 * Metodo para buscar un elememtos de manera Iterativa
 * @param ele
 * @param result
 * @return 
 * @pre Si el T es una clase debe tener implementado el operador < y >
 * tambien se utiliza el operador =
 */
template <typename T>
bool AVL<T>::buscarIt(T& ele, T* &result){
    Nodo *p= buscaClaveIt (ele,raiz);
    if (p){
        result=&(p->dato);
        return true;
    }
    return false;
}

/**
 * @brief Metodo privado Iterativo para busca en el AVL 
 * @param ele
 * @param p
 * @return 
 * @pre Si el T es una clase debe tener implementado el operador < y >
 */
template <typename T>
typename AVL<T>::Nodo* AVL<T>::buscaClaveIt(T& ele, Nodo* p) {  
    while (p) {
        if (ele > p->dato)
            p = p->der;
        else if (ele < p->dato)
            p = p->izq;
        else
            return p;
    }
      return 0;   
}


/**************************************************************************/
/***                          FIN AVL                                   ***/
/**************************************************************************/

#endif /* AVL_H */

