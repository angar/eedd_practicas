/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ListaEnlazada.h
 * Author: usuario
 *
 * Created on 5 de octubre de 2018, 10:03
 */

#ifndef LISTAENLAZADA_H
#define LISTAENLAZADA_H

#include <cstddef>// nullptr
#include <stdexcept>

template <typename T>
class ListaEnlazada {
private://hago referencia al t de plantilla, tiene que ser privado el nodo para que desde fuera de lista enlazada no se pueda acceder al nodo

    class Nodo {
    public: //TIENE QUE SER PUBLICO PARA ACCEDER DESDE LA Listaenlazada
        T dato; ///< dato del tipo T
        Nodo *sig; ///< puntero al siguient nodo
    public:
        Nodo(const T &aDato, Nodo *aSig = 0);
        Nodo(const Nodo &orig);
        Nodo& operator=(const Nodo &right);
    };

public:

    class Iterador {
    private:
        friend ListaEnlazada; //para el metodo de inserta() necesitamos acceder al nodo del iterador
        Nodo *nodo; ///<Puntero a un nodo, el nodo tiene que ser privado el usario no puede acceder al main

    public:
        Iterador ();
        
        Iterador(const Iterador &orig);
        Iterador& operator=(const Iterador &right);
        bool fin();
        void siguiente() throw(std::out_of_range);
        T &dato() throw (std::out_of_range);
    private:
        Iterador(Nodo *aNodo);
        
    }; 

public:
    ListaEnlazada();
    ListaEnlazada(const ListaEnlazada& orig);
    virtual ~ListaEnlazada();
    
    ListaEnlazada& operator=(const ListaEnlazada &right);
    
    
    void insertaInicio (T &dato);
    void insertaFin (const T &dato);
    
    void borraInicio();
    void borraFinal();
    
    void inserta (Iterador &i, T &dato) throw (std::out_of_range);
    void borra (Iterador &i) throw (std::out_of_range);
    
    int tam();
    
    T& inicio() throw (std::out_of_range);
    T& fin() throw (std::out_of_range);
    
    void borra (Iterador &i, int numElementos) throw (std::invalid_argument);
    ListaEnlazada concatena(const ListaEnlazada &l);
    
    Iterador iterador() const;
 
    private:
        void liberarLista();
        
private:
    Nodo *cabecera; ///< puntero al primero nodo
    Nodo *cola;///< puntero al ultimo nodo
    int numL;///< numero de elementos de la lista enlazada
};




/**************************************************************************/
/***                    NODO                                            ***/
/**************************************************************************/

/**
 * @brief Constructor parametrizado con valores por defecto
 * @param aDato 
 * @param aSig
 */
template<class T>
ListaEnlazada<T>::Nodo::Nodo(const T &aDato, Nodo *aSig) 
    : dato(aDato), 
    sig(aSig) {

}

/**
 * @brief Constructo copia de nodo
 * @param orig
 */
template <typename T>
ListaEnlazada<T>::Nodo::Nodo(const Nodo &orig)
    :dato(orig.dato),
    sig(orig.sig){
    
}

/**
 * @brief OPerador de asignacion de nodo
 * @param right
 * @return 
 */
template <typename T>
typename ListaEnlazada<T>::Nodo& ListaEnlazada<T>::Nodo::operator =(const Nodo& right){
    if(this != &right){
        dato=right.dato;
        sig=right.sig;
    }
    return *this;
}
/**************************************************************************/
/***                    FIN NODO                                        ***/
/**************************************************************************/







/**************************************************************************/
/***                    ITERADOR                                        ***/
/**************************************************************************/


/**
 * @brief Constructor por defecto de Iterador
 */
template <typename T>
ListaEnlazada<T>::Iterador::Iterador()
: nodo(0) {
}


/**
 * @brief Constructor parametrizado de Iterador es privado
 * @param aNodo
 */
template <typename T>
ListaEnlazada<T>::Iterador::Iterador(Nodo* aNodo)
: nodo(aNodo) {
}

/**
 * @brief Metodo para comprobar si el iterador ha llegado al final de la listaEnlazada
 * @return true si ha llegado al final, false si no ha llegado al final 
 */
template <typename T>
bool ListaEnlazada<T>::Iterador::fin(){
    return nodo==0;
}

/**
 * @brief Metodo para apuntar el iterador al siguiente elemento de la lista
 * @post Modifica el apuntador de iterador 
 * @throw  out_of_range, si el nodo es cero no podemos acceder al siguiente
 */
template <typename T>
void ListaEnlazada<T>::Iterador::siguiente() throw(std::out_of_range){
    if(nodo==nullptr)
        throw std::out_of_range("[ListaEnlazada<T>::Iterador::siguiente()]: el puntero es nulo");
    nodo = nodo->sig;
}

/**
 * @brief Metodo que nos da el dato que hay en el nodo
 * @return Una referencia al tipo de dato
 * @throw  out_of_range, si el nodo es cero no podemos acceder al siguiente
 */
template <typename T>
T& ListaEnlazada<T>::Iterador::dato() throw (std::out_of_range) {
    if (nodo == nullptr)
        throw std::out_of_range("[ListaEnlazada<T>::Iterador::dato()]: el puntero es nulo");
    return nodo->dato;
}

/**
 * @brief Constructor copia de iterador
 * @param orig
 */
template <typename T>
ListaEnlazada<T>::Iterador::Iterador(const Iterador& orig) :
nodo(orig.nodo) {

}

/**
 * @brief Operador de asignacion de Iterador
 * @param right
 * @return 
 */
template <typename T>
typename ListaEnlazada<T>::Iterador& ListaEnlazada<T>::Iterador::operator=(const Iterador& right) {
    if (this != &right) {
        nodo = right.nodo; //Necesitamos operador = de nodo Pero no entra cuando trazo
    }
    return *this;
}

/**************************************************************************/
/***                    FIN ITERADOR                                    ***/
/**************************************************************************/




/**************************************************************************/
/***                    LISTA ENLAZADA                                  ***/
/**************************************************************************/

/**
 * @brief Constructo por defecto de lista Enlazada
 */
template <class T>
ListaEnlazada<T>::ListaEnlazada()
: cabecera(0),
cola(0),
numL(0) {
}

/**
 * @brief COnstructo copia
 * Primero asigno a this cabecera, cola y numL 0 por si la lista orig estuvier vacia
 * @param orig
 */
template <typename T>
ListaEnlazada<T>::ListaEnlazada(const ListaEnlazada& orig)
: cabecera(0),
cola(0),
numL(0) {

    if (orig.cabecera != 0) {
        //Utilizo dos punteros uno para recorrer la lista de this y otro para la de orig
        Nodo *auxOrig = orig.cabecera;
        Nodo *auxThis = cabecera;

        //Bucle para recorrer toda la lista de orig
        while (auxOrig != 0) {
            if (cabecera == 0) {//Para el primer nodo de this
                cabecera = new Nodo(orig.cabecera->dato);//++numL;
                auxThis = cabecera;
                auxOrig = auxOrig->sig;
            } else {//para los nodos siguientes(todos menos el primero)
                auxThis->sig = new Nodo(auxOrig->dato);//++numL;
                auxThis = auxThis->sig;
                auxOrig = auxOrig->sig;
            }

        }
        cola = auxThis;
        numL = orig.numL;


    }
}

/**
 * @brief OPerador de asignacion
 * @param right
 * @return La lista this
 * @post Si la lista right es otra, se modifica la cabecera, cola y el numL
 */
template <typename T>
ListaEnlazada<T>& ListaEnlazada<T>::operator=(const ListaEnlazada& right) {
    if (this != &right) {
        if (right.cabecera != 0) {
            this->liberarLista();//Liberamos la lista this

            Nodo *auxOrig = right.cabecera;
            Nodo *auxThis = cabecera;

            while (auxOrig != 0) {
                if (cabecera == 0) {
                    cabecera = new Nodo(right.cabecera->dato);//++numL;
                    auxThis = cabecera;
                    auxOrig = auxOrig->sig;
                } else {
                    auxThis->sig = new Nodo(auxOrig->dato);//++numL;
                    auxThis = auxThis->sig;
                    auxOrig = auxOrig->sig;
                }

            }
            cola = auxThis;
            numL = right.numL;
            
        }else{
            cabecera=cola=0;
            numL=0;
        }
    }
    return *this;
}


/**
 * @brief Metodo privado para liberar la lista enlazada 
 */
template <typename T>
void ListaEnlazada<T>::liberarLista(){
    //ir borrando por el inicio
        if (cabecera != 0) {
        Nodo *aux=0;
        while(cabecera!=0){
            aux=cabecera->sig;
            delete cabecera;
            cabecera=aux;
        }
    }
   cola = 0;
}


/**
 * @brief Destructo de la lista enlazada
 */
template <typename T>
ListaEnlazada<T>::~ListaEnlazada(){
    
    ListaEnlazada<T>::liberarLista();
    
}


/**
 * @brief metodo para insertar por el inicio de la lista enlazada
 * Comprueba si la lista esta vacia
 * @param dato
 */
template <typename T>
void ListaEnlazada<T>::insertaInicio(T &dato){
    //Nodo<T> *nuevo= new Nodo<T>(T, cabecera);
    Nodo *nuevo= new Nodo(dato, cabecera);
    cabecera=nuevo;
    
    if(cola==0)
        cola=nuevo;
    
    ++numL;
}

/**
 * @brief Metodo para insertar por el final de la lista enlazada
 * Comprueba si la lista esta vacia
 * @param dato
 */
template <typename T>
void ListaEnlazada<T>::insertaFin (const T &dato){
    Nodo *nuevo= new Nodo (dato);// El constructor de nodo sino le asigna a quien apunta sig le asigna 0
    
    if(cola !=0){
        cola->sig=nuevo;
    }else{
        cabecera=nuevo;
    }
    cola=nuevo;
    ++numL;
}


/**
 * @brief Metodo para borrar por el inicio
 * Comprueba si la lista esta vacia
 */
template <typename T>
void ListaEnlazada<T>::borraInicio(){
    if(cabecera!=0){
        Nodo *borrado=cabecera;
        cabecera=cabecera->sig;
        delete borrado;
        --numL;
        if(cabecera == 0)
            cola=0;
    }
}

/**
 * @brief MEtodo para borrar por el final el nodo
 * Si estuviera vacia hace delete de 0 
 */
template <typename T>
void ListaEnlazada<T>::borraFinal() {
    Nodo *auxAnt = 0;

    if (cabecera != cola) {
        auxAnt = cabecera;
        while (auxAnt->sig != cola) {
            auxAnt = auxAnt->sig;
        }
    }
    
    delete cola;
    if (cola!=0)// si la lista estuviera vacia, asi no resta 
        --numL;
    
    cola = auxAnt;
    if(auxAnt !=0)
        auxAnt->sig=0;
    else
        cabecera=0;
}


/**
 * Metodo para insertar un dato en la lista empleando un iterador
 * Si el iterador es cero y la lista esta vacia, se inserta el dato en la lista
 * Si el iterador es cero y la lista tiene mas de un elemento, lanza excepcion
 * @param i
 * @param dato
 * @pre El iterador debe ser de la misma lista enlazada
 * @throw out_of_range, si el iterador es cero y la lista tiene al menos un elemento
 */
template <class T>
void ListaEnlazada<T>::inserta(Iterador& i, T& dato) throw (std::out_of_range){
    //Se puede dar que la lista este vacia y el iterador sea cero
    if (i.nodo != 0 || cabecera == 0) {//si el iterador es cero y la lista no tiene elementos inserta
        Nodo *anterior = 0;
        Nodo *nuevo = 0;
        if (cabecera != cola) {
            anterior = cabecera;
            while (anterior->sig != i.nodo)
                anterior = anterior->sig;

            nuevo = new Nodo(dato, i.nodo);
            anterior->sig = nuevo;
            ++numL;

        } else {
            //Parte que solo hay un nodo o que este vacia la lista
            nuevo = new Nodo(dato, i.nodo);
            ++numL;

            if (cabecera == nullptr) {
                cabecera = cola = nuevo;
            } else {
                cabecera = nuevo;
            }
        }
    } else//si el nodo es cero y la lista tiene al menos un elementos
        throw std::out_of_range("[ListaEnlazada<T>::inserta]: iterador no valido");

}

/**
 * @brief MEtodo para borrar dado un iterador
 * @param i
 * @pre El iterador debe ser la la misma lista enlaza y distinto de cero
 * @throw out_of_range, si el iterador es cero 
 */
template<typename T>
void ListaEnlazada<T>::borra(Iterador& i) throw (std::out_of_range){
    if (i.nodo != 0) {
        Nodo *anterior = 0;
        if (cabecera != cola) {
            anterior = cabecera;
            while (anterior->sig != i.nodo)
                anterior = anterior->sig;
        }
        anterior->sig=i.nodo->sig;
        delete i.nodo;
        --numL;
         
        
    }else
        throw std::out_of_range("ListaEnlazada<T>::borra(Iterador& i): iterador no valido");
}

/**
 * @brief Metodo para dar el tamaño de la lista enlazada
 * @return numero de datos de la lista
 */
template <typename T>
int ListaEnlazada<T>::tam(){
    return numL;
}

/**
 * @brief Metodo que nos da el dato de la cabecera
 * @return Referencia al tipo de dato de la cabecera
 * @throw out_of_range, si la lista esta vacia
 */
template <typename T>
T& ListaEnlazada<T>::inicio() throw (std::out_of_range){
    if (cabecera==0)
        throw std::out_of_range("[ListaEnlazada<T>::inicio]: lista vacia");
    return cabecera->dato;
}

/**
 * @brief Metodo que nos da el dato de la cola
 * @return Referencia al tipo de dato de la cabecera
 * @throw out_of_range, si la lista esta vacia
 */
template <typename T>
T& ListaEnlazada<T>::fin() throw (std::out_of_range){
    if (cola==0)
        throw std::out_of_range("[ListaEnlazada<T>::fin]: lista vacia");
    return cola->dato;
}


/**
 * @brief Metodo para borrar dado el iterado y el numero de elementos
 * Comprueba que no ha llegado al final para ver que no se pueden borrar mas elementos
 * de los que hay en la lista enlazada
 * 
 * @param i
 * @param numElementos
 * @pre El iterador debe ser de la misma lista y distinto de cero
 * @throw invalid_argument, si el iterador es cero
 */
template <typename T>
void ListaEnlazada<T>::borra(Iterador& i, int numElementos) throw (std::invalid_argument){
    if (i.nodo==0)
        throw std::invalid_argument("ListaEnlazada<T>::borra: iterador no valido");
    
    //antes de borrar tengo que dejar apuntado el iterador al siguiente
    Iterador borrado(i.nodo);
    for (int j=0; j<numElementos &&  !i.fin() ;j++){
        i.siguiente();
        this->borra(borrado);//metodo borra, decrementa el numero de elementos
        borrado=i;
    }
    

}


/**
 * @brief MEtodo para concatenar la lista this y otra pasada por referencia
 * No tenemos que incrementar el numero de elementos lo hace el constructor copia y el insertaFIn
 * @param l
 * @return UNa copia de la lista concatenada, debe ser una copia y no una referencia porque si fuera una 
 * referencia la lista resultado se crearia en la pila y se destruiria al finalizar el metodo 
 */
template <typename T>
ListaEnlazada<T> ListaEnlazada<T>::concatena(const ListaEnlazada& l){
    ListaEnlazada result(*this); //mejor utilizar constructor copia que asigancion(result=*this);
    ListaEnlazada<T>::Iterador itL = l.iterador();//no funcionaba porque el metodo iterador tenia que ser const //Iterador itL(l.cabecera);
    while (!itL.fin()){
        result.insertaFin(itL.dato());
        itL.siguiente();
    }
    return result;
    
}


/**
 * @brief Metodo que devuelve un iterador de la lista enlazada
 * @return 
 */
template <typename T>
typename ListaEnlazada<T>::Iterador ListaEnlazada<T>::iterador() const{
    return Iterador (cabecera);
}
#endif /* LISTAENLAZADA_H */

