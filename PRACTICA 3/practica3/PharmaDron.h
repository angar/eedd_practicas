/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PharmaDron.h
 * Author: usuario
 *
 * Created on 8 de noviembre de 2018, 11:28
 */

#ifndef PHARMADRON_H
#define PHARMADRON_H


#include <string>
#include <fstream> //std::ifstream (lectura fichero)
#include<stdexcept> //std::invalid_argument

#include "AVL.h"
#include "Cliente.h"
#include "ListaEnlazada.h"


class Cliente;

class PharmaDron {
public:
    PharmaDron();
    PharmaDron(const PharmaDron& orig);
    virtual ~PharmaDron();
    PharmaDron& operator =(const PharmaDron right);
    
    void cargaProductos(std::string &fileNameProductos);
    void crearClientes (std::string &fileNameClientes);
    void aniadeProducto(Producto &p);//puede ser por referencia o copia porque añadimos al sistema
    void nuevoCliente (Cliente &c);//puede ser por referencia o copia porque añadimos al sistema
    ListaEnlazada<Producto *> buscaProducto(std::string &subcadena);
    Cliente* ingresaCliente(std::string dni, std::string pass="");
    Pedido& verPedido(Cliente &c) ; 
    
    int numClientes();
    void visualizarClientes();
    
    int numProductos();
    
    
    
private:
    
    ListaEnlazada<Producto> productos;///<Composicion de productos EEDD ListaEnlazada
    AVL<Cliente> clientes;///<Composicion de clientes EEDD AVL
    UTM posicion;///< Posicion del PharmaDron
    
};

#endif /* PHARMADRON_H */

