/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliente.cpp
 * Author: usuario
 * 
 * Created on 22 de octubre de 2018, 22:05
 */

#include "Cliente.h"
#include "ListaEnlazada.h"

/**
 * @brief Constructor por defecto parametrizado
 * Sino se asigna a ningun campo los campos tendran la cadena de caracteres vacia
 * 
 * @param _dni Dni del cliente
 * @param _nombre Nombre del cliente
 * @param _pass Contraseña del cliente
 * @param _direccion Direccion del cliente
 */
Cliente::Cliente(std::string _dni ,std::string _nombre, std::string _pass, std::string _direccion,float latit, float longi, PharmaDron *phar)
    : dni(_dni),
    nombre(_nombre),
    pass(_pass),
    direccion(_direccion),
    posicion(latit,longi),
    pharma(phar),
    pedidos(){

}

/**
 * @brief Constructor parametrizado 
 * Crea un cliente asociandole un PharmaDron
 * @param phar
 */
Cliente::Cliente(PharmaDron *phar)
    :dni(""),
    nombre(""),
    pass(""),
    direccion(""),
    posicion(),
    pharma(phar),
    pedidos(){
    
}



/**
 * @brief Constructor copia
 * @param orig
 */
Cliente::Cliente(const Cliente& orig)
    :dni(orig.dni),
    nombre(orig.nombre),
    pass(orig.pass),
    direccion(orig.direccion),
    posicion(orig.posicion),
    pharma(orig.pharma),
    pedidos(orig.pedidos){
}

/**
 * @brief Operador de asignacion 
 * @param right
 * @return 
 */
Cliente& Cliente::operator =(const Cliente& right){
    if(this != &right){
        dni=right.dni;
        nombre=right.nombre;
        pass=right.pass;
        direccion=right.direccion;
        posicion=right.posicion;
        pharma=right.pharma;
        pedidos=right.pedidos;
    }
    return *this;
}

void Cliente::SetDireccion(std::string direccion) {
    this->direccion = direccion;
}

std::string Cliente::GetDireccion() const {
    return direccion;
}

void Cliente::SetPass(std::string pass) {
    this->pass = pass;
}

std::string Cliente::GetPass() const {
    return pass;
}

void Cliente::SetNombre(std::string nombre) {
    this->nombre = nombre;
}

std::string Cliente::GetNombre() const {
    return nombre;
}

void Cliente::SetDni(std::string dni) {
    this->dni = dni;
}

std::string Cliente::GetDni() const {
    return dni;
}

Cliente::~Cliente() {
}

/**
 * @brief Representacion CVS de un cliente
 * @return std::string 
 */
std::string Cliente::toCSV() const{
    std::stringstream aux;
    
    aux<< dni   << ";"
       << nombre<< ";"
       << pass  << ";"
       <<direccion<< ";"
       <<posicion.toCSV();
    
    return aux.str();
}



/**
 * @brief Asigna informacion a un cliente a partir de su representacion CSV
 * @param linea 
 */
void Cliente::fromCSV(std::string& linea){
    std::stringstream cli(linea); 
    std::string campo; 

    //Leemos el dni
    std::getline(cli, dni, ';');

    //Leemos la contraseña
    std::getline(cli, pass, ';');
    //Leemos el nombre
    std::getline(cli, nombre, ';');
    
    std::getline(cli, direccion, ';');
    //std::cout<<"linea: "<<cli.str()<<std::endl;
    //posicion.fromCSV(cli.str());
    //TODO mejor seria llamar al fromCSV de UTM posicion.fromCSV(cli)
    //No funciona llamar el metod fromCSV de UTM porque 
    std::getline(cli, campo, ';');
    try {
        posicion.SetLatitud(std::stof(campo));
    } catch (std::invalid_argument &e) {
        posicion.SetLatitud(0);
    }

    std::getline(cli, campo, ';');
    try {
        posicion.SetLongitud(std::stof(campo));
    } catch (std::invalid_argument &e) {
        posicion.SetLongitud(0);
    }
    
    
}



/**
 * @brief Metodo para añadir un nuevo producto al pedido del cliente
 * es un puntero porque queremos incluir el producto que ya existe en el sistema
 * @param p
 * @post Si p es un puntero nulo, no inserta el producto
 */
void Cliente::addProductoPedido(Producto* p) {
    if (p != 0)
        pedidos.nuevoProducto(p);
}






/**
 * @brief Metodo para buscar productos en el PharmaDron dada una subcadena
 * Devuelvo una listaENlazada de punteros a producto porque quiero los productos 
 * del sistema, no devuelvo la lista por referencia porque se crea en pila,
 * hay que devolver una copia
 * @param subcadena
 * @return 
 * @post si subcadena es vacia, devuelve una lista vacia
 */
ListaEnlazada<Producto *> Cliente::buscarProducto(std::string& subcadena){
    //ListaEnlazada<Producto*> listaProduc;
    //VDinamico<Producto *> vecProduc;
    
   return pharma->buscaProducto(subcadena);

}





/**
 * @brief Sobrecarga del operador <
 * @param right
 * @return 
 */
bool Cliente::operator<(const Cliente& right) const {
    return this->dni < right.dni;
}

/**
 * @brief Sobrecarga del operador ==
 * @param right
 * @return 
 */
bool Cliente::operator==(const Cliente& right) const {
    return this->dni == right.dni;
}

/**
 * @brief Sobrecarga del operador <=
 * @param right
 * @return 
 * @pre Se necesita el operador < y el ==
 */
bool Cliente::operator<=(const Cliente& right) const {
    return (this->operator <(right) || (*this==right) );
}


/**
 * @brief Sobrecarga del operador >
 * @param right
 * @return 
 * @pre Se necesita el operador <=
 */
bool Cliente::operator>(const Cliente& right) const {
    return (!(*this <=right));
}

/**
 * @brief Metodo observador de pedidos
 * @return 
 */
Pedido& Cliente::getPedidos(){
    return pedidos;
}


/**
 * @brief Sobrecarga de operado << para imprimir por pantalla en el AVL el dato Cliente
 * @param out
 * @param c
 * @return 
 */
std::ostream& operator<<(std::ostream& out, const Cliente& c) {
    return out << c.toCSV();
}

